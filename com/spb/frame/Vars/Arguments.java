package com.spb.frame.Vars;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ares7 on 19.03.2017.
 */
public class Arguments {
    Map<String, String> args = new HashMap<>();

    public Arguments(String[] args) {
        for(String arg : args) {
            System.out.println("Found argument type " + arg);
            try {
                this.args.put(arg.split("=")[0], arg.split("=")[1]);
            }
            catch(Exception e) {
                // error handling lul...
            }
        }
    }
}
