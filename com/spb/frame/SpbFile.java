package com.spb.frame;

import java.io.*;

/**
 * Created by ares7 on 19.02.2017.
 */
public class SpbFile {
    public static String base = "C:\\data\\Spb\\";

    public static void write(String path, String value, boolean append) {
        try {
            prepare(path);
            FileWriter writer = new FileWriter(base + path, append);
            writer.write(value);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String root() {
        try {
            return new File( "." ).getCanonicalPath();
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String read(String path) {
        String s = "";
        try {
            FileReader reader = new FileReader(base + path);
            BufferedReader bufferedReader = new BufferedReader(reader);
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                s += line;
            }
            reader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return s;
    }

    public static void delete(String path) {
        (new File(path)).delete();
    }

    public static void prepare(String path) {
        File f = new File(base + path);
        String dirOnly = "";
        for(int i = 0; i < (path.split("/").length) - 1; i ++) {
            dirOnly += "/" + path.split("/")[i];
        }
        File dir = new File(dirOnly);

        if(!dir.exists())
            dir.mkdirs();
        f.setReadable(true);
        f.setWritable(true);
        if(!f.exists())
        {
            try {
                f.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static boolean exists(String path) {
        return (new File(base + path)).exists();
    }
}
