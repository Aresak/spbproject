package com.spb.frame;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

/**
 * Created by ares7 on 18.02.2017.
 */
public class Utils {
    public static HashMap<String, String> GetURLParams(String url) {
        HashMap<String, String> res = new HashMap<>();
        for(String param : url.split("\\?")[1].split("&")) {
            System.out.println("Param: " + param);
            if(!param.contains("=")) {
                res.put(param, "");
                continue;
            }
            res.put(param.split("=")[0], param.split("=")[1]);
        }
        return res;
    }

    public static void waitForLoad(WebDriver driver) {
        //driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        new WebDriverWait(driver, 30).until((ExpectedCondition<Boolean>) wd ->
                ((JavascriptExecutor) wd).executeScript("return document.readyState").equals("complete"));
    }

    public static boolean isElementPresent(WebDriver driver, By locatorKey) {
        try {
            (new WebDriverWait(driver, 1)).until(ExpectedConditions.presenceOfElementLocated(locatorKey));
            return true;
        } catch (Exception e) {
            System.err.println("Locator " + locatorKey.toString() + " is not present at page - ignoring");
            return false;
        }
    }
}
