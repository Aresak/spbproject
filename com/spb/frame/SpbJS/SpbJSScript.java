package com.spb.frame.SpbJS;

import javax.script.Bindings;

/**
 * Created by ares7 on 22.02.2017.
 */
public class SpbJSScript {
    public String author = "";
    public String name = "";
    public double version = 0;
    public String description = "";

    public String script = "";
    public String path = "";


    public SpbJSScript(Bindings props, String script) {
        this.script = script;

        if(props.containsKey("author"))
            author = (String) props.get("author");

        if(props.containsKey("name"))
            name = (String) props.get("name");

        if(props.containsKey("version"))
            version = (Double) props.get("version");

        if(props.containsKey("description"))
            description = (String) props.get("description");

        if(props.containsKey("path"))
            path = (String) props.get("script");
    }
}
