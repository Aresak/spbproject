package com.spb.frame.TribalWars;

import com.spb.frame.SmartBlock;
import com.spb.frame.SpbFile;
import com.spb.frame.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


/**
 * Created by ares7 on 18.02.2017.
 */
public class TribalWars {
    public static TribalWars self;
    public static String username = "Spb48";
    public static String password = "koyot40";
    public static String world = "css1";
    public static String domain = "divokekmeny.cz";
    public static boolean _UseFileAuth = false;
    public WebDriver driver;
    public JavascriptExecutor js;

    public int focusedVillage = 0;


    public TWResources resources;

    // Game Data
    public String majorVersion = "";
    public boolean accountManager = false;
    public boolean farmManager = false;
    public boolean premium = false;
    public boolean emailValid = false;
    public int playerId = 0;
    public int playerPoints = 0;
    public int playerRank = 0;
    public int villages = 0;



    public TribalWars(WebDriver driver) {
        this.driver = driver;
        resources = new TWResources(this);
        self = this;

        js = (JavascriptExecutor) driver;

        if(_UseFileAuth) {
            if(SpbFile.exists("tribalwars.sb")) {
                SmartBlock auth = new SmartBlock("FileAuth");
                auth.process(SpbFile.read("tribalwars.sb"));
                TribalWars.username = auth.getProperty("Username");
                TribalWars.password = auth.getProperty("Password");
                TribalWars.world = auth.getProperty("World");
                TribalWars.domain = auth.getProperty("Domain");
            } else {
                SmartBlock auth = new SmartBlock("FileAuth");
                auth.addProperty("Username", "YourUsernameHere");
                auth.addProperty("Password", "YourPasswordHere");
                auth.addProperty("World", "YourWorldHere");
                auth.addProperty("Domain", "TribalWarsDomain");
                SpbFile.write("tribalwars.sb", auth.process(), false);
                System.err.println("Go to " + SpbFile.base + "tribalwars.sb and configure your FileAuth!");
                System.exit(69);
            }
        }

        driver.findElement(By.id("user")).sendKeys(username);
        WebElement password = driver.findElement(By.id("password"));
        password.sendKeys(this.password);
        password.submit();
        Utils.waitForLoad(driver);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.navigate().to("https://" + domain + "/page/play/" + world);
        Utils.waitForLoad(driver);

        if(Utils.isElementPresent(driver, By.className("btn"))) {
            driver.findElement(By.className("btn")).click();
            Utils.waitForLoad(driver);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }



        if(Utils.isElementPresent(driver, By.className("popup_box_close")))
            driver.findElement(By.className("popup_box_close")).click();

        JavascriptExecutor js = (JavascriptExecutor) driver;
        majorVersion = js.executeScript("return game_data.majorVersion").toString();
        accountManager = Boolean.parseBoolean(js.executeScript("return game_data.player.account_manager").toString());
        farmManager = Boolean.parseBoolean(js.executeScript("return game_data.player.farm_manager").toString());
        premium = Boolean.parseBoolean(js.executeScript("return game_data.player.premium").toString());
        emailValid = Boolean.parseBoolean(js.executeScript("return game_data.player.email_valid").toString());
        playerId = Integer.parseInt(js.executeScript("return game_data.player.id").toString());
        playerPoints = Integer.parseInt(js.executeScript("return game_data.player.points").toString());
        playerRank = Integer.parseInt(js.executeScript("return game_data.player.rank").toString());
        villages = Integer.parseInt(js.executeScript("return game_data.player.villages").toString());

        Utils.waitForLoad(driver);
        resources.Init();
    }

    public String[] listAllWorlds() {
        return null;
    }

    public String[] listActiveWorlds() {
        return null;
    }

    public String[] listInactiveWorlds() {
        return null;
    }

    public String[] listSpeedWorlds() {
        return null;
    }

    public void Screen(String screen) {
        driver.navigate().to(URL_ExcludeProp(driver.getCurrentUrl(), "screen") + "&screen=" + screen);
        Utils.waitForLoad(driver);
    }

    public void Screen(String screen, String mode) {

    }


    public String URL_ExcludeProp(String url, String prop) {
        String[] t = url.split("\\?");
        String[] tp = t[1].split("&");

        String a = t[0];
        int b = 0;
        for(String p : tp) {
            if(!p.split("=")[0].toLowerCase().equals(prop.toLowerCase())) {
                a += (b == 0 ? "?" : "&") + p;
                b++;
            }
        }
        return a;
    }

    public String URL_ExcludeProp(String url, String[] prop) {
        String[] t = url.split("\\?");
        String[] tp = t[1].split("&");

        String a = t[0];
        int b = 0;
        for(String p : tp) {
            for(String rp  : prop) {
                if(!p.split("=")[0].toLowerCase().equals(rp.toLowerCase())) {
                    a += (b == 0 ? "?" : "&") + p;
                    b ++;
                }
                else
                    break;
            }
        }
        return a;
    }

    public static class Screens {
        public static String Overview = "overview";
        public static String HQ = "main";
        public static String Church_F = "church_f";
        public static String Storage = "storage";
        public static String Map = "map";
        public static String Reports_All;
        public static String Reports_Attack;
        public static String Reports_Defense;
        public static String Reports_Support;
        public static String Reports_Trade;
        public static String Reports_Events;
        public static String Reports_Other;
        public static String Reports_Forwarded;
        public static String Reports_Public;
        public static String Reports_Filter;
        public static String Mail;
        public static String Mail_Mass;
        public static String Mail_New;
        public static String Ranking_Ally;
        public static String Ranking_Player;
        public static String Ranking_Dominance;
        public static String Ranking_ConAlly;
        public static String Ranking_ConPlayer;
        public static String Ranking_KillAlly;
        public static String Ranking_KillPlayer;
        public static String Ranking_Awards;
        public static String Ranking_Day;
        public static String Ranking_Wars;
        public static String Ally;
        public static String Info_Player;
        public static String Settings_Main;
        public static String Settings_Account;
        public static String Settings_Move;
        public static String Settings_Casual;
        public static String Settings_Quickbar;
        public static String Settings_Share;
        public static String Settings_Vacation;
        public static String Settings_Logins;
        public static String Settings_Poll;
        public static String Settings_Ref;
        public static String Settings_Email;
        public static String Settings_CommandSharing;
        public static String Settings_NotesSharing;
        public static String Flags;
        public static String Notes;
        public static String Place = "place";
        public static String Hide = "hide";
        public static String Wood = "wood";
        public static String Iron = "iron";
        public static String Stone = "stone";
        public static String Farm = "farm";
        public static String Barracks = "barracks";
        public static String Stable = "stable";
        public static String Garage = "garage";
        public static String Watch_Tower = "watchtower";
        public static String Snob = "snob";
        public static String Smith = "smith";
        public static String Market = "market";
        public static String Wall = "wall";
        public static String Train = "train";
        public static String Player_Inventory;
        public static String Player_Awards;
        public static String Player_Stats;
        public static String Player_Friends;
        public static String Player_DailyBonus;
        public static String Player_Mentor;
        public static String Player_Blocked;
        public static String Premium_Use;
        public static String Premium_Buy;
        public static String Premium_Transfer;
        public static String Premium_Log;
        public static String Premium_Features;
    }
}
