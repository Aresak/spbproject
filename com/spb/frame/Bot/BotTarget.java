package com.spb.frame.Bot;

import com.spb.frame.SmartBlock;
import com.spb.frame.SpbFile;
import com.spb.frame.TribalWars.Village;

import java.util.LinkedList;

/**
 * Created by ares7 on 19.02.2017.
 */
public class BotTarget {
    public Village village;
    public String customTargetName;
    public long hash;

    public LinkedList<Integer> assignedReports = new LinkedList<>();

    public BotTarget(Village village) {
        this.village = village;
        hash = village.id + village.coords_x + village.coords_y;
    }


    public int R_totalWood = 0;
    public int R_totalStone = 0;
    public int R_totalIron = 0;
    public int R_totalRaids = 0;


    public int getAverageWood() {
        return R_totalWood / R_totalRaids;
    }

    public int getAverageStone() {
        return R_totalStone / R_totalRaids;
    }

    public int getAverageIron() {
        return R_totalIron / R_totalRaids;
    }

    public void SaveTarget() {
        SmartBlock sb = new SmartBlock("BotTarget");
        sb.addProperty("village_x", village.coords_x);
        sb.addProperty("village_y", village.coords_y);
        sb.addProperty("village_name", village.name);
        sb.addProperty("village_id", village.id);
        sb.addProperty("village_point", village.point);
        sb.addProperty("target_custom_name", customTargetName);
        sb.addProperty("target_hash", hash);
        sb.addProperty("target_total_wood", R_totalWood);
        sb.addProperty("target_total_stone", R_totalStone);
        sb.addProperty("target_total_iron", R_totalIron);
        sb.addProperty("target_total_raids", R_totalRaids);
        sb.addProperty("reports_count", assignedReports.size());
        int i = 0;
        for(Integer a : assignedReports) {
            sb.addProperty("report-" + i, a);
            i ++;
        }

        SpbFile.write("BotTargets\\target_" + hash + ".sb", sb.process(), false);
    }
}
