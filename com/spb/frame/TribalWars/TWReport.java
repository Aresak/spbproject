package com.spb.frame.TribalWars;

import com.spb.frame.SmartBlock;
import com.spb.frame.SpbFile;
import com.spb.frame.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.Date;
import java.util.List;

/**
 * Created by ares7 on 19.02.2017.
 */
public class TWReport {
    public Village attacker_vil;
    public Village defenser_vil;

    public int id;


    public String attacker = "";
    public String defenser = "";
    public String winner = "";
    public float luck = 0;

    public int A_Spear = 0;
    public int A_Sword = 0;
    public int A_Axe = 0;
    public int A_Bow = 0;
    public int A_Spy = 0;
    public int A_Light = 0;
    public int A_MArcher = 0;
    public int A_Heavy = 0;
    public int A_Ram = 0;
    public int A_Catapult = 0;
    public int A_Knight = 0;
    public int A_Snob = 0;
    public int A_Spear_L = 0;
    public int A_Sword_L = 0;
    public int A_Axe_L = 0;
    public int A_Bow_L = 0;
    public int A_Spy_L = 0;
    public int A_Light_L = 0;
    public int A_MArcher_L = 0;
    public int A_Heavy_L = 0;
    public int A_Ram_L = 0;
    public int A_Catapult_L = 0;
    public int A_Knight_L = 0;
    public int A_Snob_L = 0;


    public int D_Spear = 0;
    public int D_Sword = 0;
    public int D_Axe = 0;
    public int D_Bow = 0;
    public int D_Spy = 0;
    public int D_Light = 0;
    public int D_MArcher = 0;
    public int D_Heavy = 0;
    public int D_Ram = 0;
    public int D_Catapult = 0;
    public int D_Knight = 0;
    public int D_Snob = 0;
    public int D_Spear_L = 0;
    public int D_Sword_L = 0;
    public int D_Axe_L = 0;
    public int D_Bow_L = 0;
    public int D_Spy_L = 0;
    public int D_Light_L = 0;
    public int D_MArcher_L = 0;
    public int D_Heavy_L = 0;
    public int D_Ram_L = 0;
    public int D_Catapult_L = 0;
    public int D_Knight_L = 0;
    public int D_Snob_L = 0;
    public int D_Spear_O = 0;
    public int D_Sword_O = 0;
    public int D_Axe_O = 0;
    public int D_Bow_O = 0;
    public int D_Spy_O = 0;
    public int D_Light_O = 0;
    public int D_MArcher_O = 0;
    public int D_Heavy_O = 0;
    public int D_Ram_O = 0;
    public int D_Catapult_O = 0;
    public int D_Knight_O = 0;
    public int D_Snob_O = 0;

    public int DR_wood = 0;
    public int DR_stone = 0;
    public int DR_iron = 0;

    public int DB_main = 0;
    public int DB_barracks = 0;
    public int DB_stable = 0;
    public int DB_garage = 0;
    public int DB_smith = 0;
    public int DB_place = 0;
    public int DB_market = 0;
    public int DB_wood = 0;
    public int DB_stone = 0;
    public int DB_iron = 0;
    public int DB_farm = 0;
    public int DB_storage = 0;
    public int DB_hide = 0;
    public int DB_wall = 0;
    public int DB_statue = 0;
    public int DB_church = 0;
    public int DB_churchF = 0;
    public int DB_snob = 0;

    public int K_wood = 0;
    public int K_stone = 0;
    public int K_iron = 0;
    public int K_off = 0;

    public Date date;

    public void SaveReport() {

        SmartBlock rep = new SmartBlock("Report");
        rep.addProperty("id", id);
        
        
        
        rep.addProperty("A_Spear", A_Spear);
        rep.addProperty("A_Sword", A_Sword);
        rep.addProperty("A_Axe", A_Axe);
        rep.addProperty("A_Bow", A_Bow);
        rep.addProperty("A_Spy", A_Spy);
        rep.addProperty("A_Light", A_Light);
        rep.addProperty("A_MArcher", A_MArcher);
        rep.addProperty("A_Heavy", A_Heavy);
        rep.addProperty("A_Ram", A_Ram);
        rep.addProperty("A_Catapult", A_Catapult);
        rep.addProperty("A_Knight", A_Knight);
        rep.addProperty("A_Snob", A_Snob);

        rep.addProperty("A_Spear_L", A_Spear_L);
        rep.addProperty("A_Sword_L", A_Sword_L);
        rep.addProperty("A_Axe_L", A_Axe_L);
        rep.addProperty("A_Bow_L", A_Bow_L);
        rep.addProperty("A_Spy_L", A_Spy_L);
        rep.addProperty("A_Light_L", A_Light_L);
        rep.addProperty("A_MArcher_L", A_MArcher_L);
        rep.addProperty("A_Heavy_L", A_Heavy_L);
        rep.addProperty("A_Ram_L", A_Ram_L);
        rep.addProperty("A_Catapult_L", A_Catapult_L);
        rep.addProperty("A_Knight_L", A_Knight_L);
        rep.addProperty("A_Snob_L", A_Snob_L);


        rep.addProperty("D_Spear", D_Spear);
        rep.addProperty("D_Sword", D_Sword);
        rep.addProperty("D_Axe", D_Axe);
        rep.addProperty("D_Bow", D_Bow);
        rep.addProperty("D_Spy", D_Spy);
        rep.addProperty("D_Light", D_Light);
        rep.addProperty("D_MArcher", D_MArcher);
        rep.addProperty("D_Heavy", D_Heavy);
        rep.addProperty("D_Ram", D_Ram);
        rep.addProperty("D_Catapult", D_Catapult);
        rep.addProperty("D_Knight", D_Knight);
        rep.addProperty("D_Snob", D_Snob);

        rep.addProperty("D_Spear_L", D_Spear_L);
        rep.addProperty("D_Sword_L", D_Sword_L);
        rep.addProperty("D_Axe_L", D_Axe_L);
        rep.addProperty("D_Bow_L", D_Bow_L);
        rep.addProperty("D_Spy_L", D_Spy_L);
        rep.addProperty("D_Light_L", D_Light_L);
        rep.addProperty("D_MArcher_L", D_MArcher_L);
        rep.addProperty("D_Heavy_L", D_Heavy_L);
        rep.addProperty("D_Ram_L", D_Ram_L);
        rep.addProperty("D_Catapult_L", D_Catapult_L);
        rep.addProperty("D_Knight_L", D_Knight_L);
        rep.addProperty("D_Snob_L", D_Snob_L);

        rep.addProperty("D_Spear_O", D_Spear_O);
        rep.addProperty("D_Sword_O", D_Sword_O);
        rep.addProperty("D_Axe_O", D_Axe_O);
        rep.addProperty("D_Bow_O", D_Bow_O);
        rep.addProperty("D_Spy_O", D_Spy_O);
        rep.addProperty("D_Light_O", D_Light_O);
        rep.addProperty("D_MArcher_O", D_MArcher_O);
        rep.addProperty("D_Heavy_O", D_Heavy_O);
        rep.addProperty("D_Ram_O", D_Ram_O);
        rep.addProperty("D_Catapult_O", D_Catapult_O);
        rep.addProperty("D_Knight_O", D_Knight_O);
        rep.addProperty("D_Snob_O", D_Snob_O);

        SpbFile.write("Reports\\report_" + id + ".sb", rep.process(), false);
    }

    public TWReport(WebDriver driver) {
        List<WebElement> el_Spear = driver.findElements(By.className("unit-item-spear"));
        List<WebElement> el_Sword = driver.findElements(By.className("unit-item-sword"));
        List<WebElement> el_Axe = driver.findElements(By.className("unit-item-axe"));
        List<WebElement> el_Bow = driver.findElements(By.className("unit-item-archer"));
        List<WebElement> el_Spy = driver.findElements(By.className("unit-item-spy"));
        List<WebElement> el_Light = driver.findElements(By.className("unit-item-light"));
        List<WebElement> el_MArcher = driver.findElements(By.className("unit-item-marcher"));
        List<WebElement> el_Heavy = driver.findElements(By.className("unit-item-heavy"));
        List<WebElement> el_Ram = driver.findElements(By.className("unit-item-ram"));
        List<WebElement> el_Catapult = driver.findElements(By.className("unit-item-catapult"));
        List<WebElement> el_Knight = driver.findElements(By.className("unit-item-knight"));
        List<WebElement> el_Snob = driver.findElements(By.className("unit-item-snob"));

        if(Utils.isElementPresent(driver, By.id("attack_spy_resources"))) {
            List<WebElement> spiedResources = driver.findElement(By.id("attack_spy_resources")).findElements(By.tagName("td"));
            String spiedBuildings = driver.findElement(By.id("attack_spy_building_data")).getAttribute("value");
        }

        id = Integer.parseInt(Utils.GetURLParams(driver.getCurrentUrl()).get("view"));
        
        // attacker
        A_Spear = Integer.parseInt(el_Spear.get(0).getText());
        A_Sword = Integer.parseInt(el_Sword.get(0).getText());
        A_Axe = Integer.parseInt(el_Axe.get(0).getText());
        A_Bow = Integer.parseInt(el_Bow.get(0).getText());
        A_Spy = Integer.parseInt(el_Spy.get(0).getText());
        A_Light = Integer.parseInt(el_Light.get(0).getText());
        A_MArcher = Integer.parseInt(el_MArcher.get(0).getText());
        A_Heavy = Integer.parseInt(el_Heavy.get(0).getText());
        A_Ram = Integer.parseInt(el_Ram.get(0).getText());
        A_Catapult = Integer.parseInt(el_Catapult.get(0).getText());
        A_Knight = Integer.parseInt(el_Knight.get(0).getText());
        A_Snob = Integer.parseInt(el_Snob.get(0).getText());

        A_Spear_L = Integer.parseInt(el_Spear.get(1).getText());
        A_Sword_L = Integer.parseInt(el_Sword.get(1).getText());
        A_Axe_L = Integer.parseInt(el_Axe.get(1).getText());
        A_Bow_L = Integer.parseInt(el_Bow.get(1).getText());
        A_Spy_L = Integer.parseInt(el_Spy.get(1).getText());
        A_Light_L = Integer.parseInt(el_Light.get(1).getText());
        A_MArcher_L = Integer.parseInt(el_MArcher.get(1).getText());
        A_Heavy_L = Integer.parseInt(el_Heavy.get(1).getText());
        A_Ram_L = Integer.parseInt(el_Ram.get(1).getText());
        A_Catapult_L = Integer.parseInt(el_Catapult.get(1).getText());
        A_Knight_L = Integer.parseInt(el_Knight.get(1).getText());
        A_Snob_L = Integer.parseInt(el_Snob.get(1).getText());


        // defender
        if(el_Spear.size() > 2) {
            D_Spear = Integer.parseInt(el_Spear.get(2).getText());
            D_Sword = Integer.parseInt(el_Sword.get(2).getText());
            D_Axe = Integer.parseInt(el_Axe.get(2).getText());
            D_Bow = Integer.parseInt(el_Bow.get(2).getText());
            D_Spy = Integer.parseInt(el_Spy.get(2).getText());
            D_Light = Integer.parseInt(el_Light.get(2).getText());
            D_MArcher = Integer.parseInt(el_MArcher.get(2).getText());
            D_Heavy = Integer.parseInt(el_Heavy.get(2).getText());
            D_Ram = Integer.parseInt(el_Ram.get(2).getText());
            D_Catapult = Integer.parseInt(el_Catapult.get(2).getText());
            D_Knight = Integer.parseInt(el_Knight.get(2).getText());
            D_Snob = Integer.parseInt(el_Snob.get(2).getText());

            D_Spear_L = Integer.parseInt(el_Spear.get(3).getText());
            D_Sword_L = Integer.parseInt(el_Sword.get(3).getText());
            D_Axe_L = Integer.parseInt(el_Axe.get(3).getText());
            D_Bow_L = Integer.parseInt(el_Bow.get(3).getText());
            D_Spy_L = Integer.parseInt(el_Spy.get(3).getText());
            D_Light_L = Integer.parseInt(el_Light.get(3).getText());
            D_MArcher_L = Integer.parseInt(el_MArcher.get(3).getText());
            D_Heavy_L = Integer.parseInt(el_Heavy.get(3).getText());
            D_Ram_L = Integer.parseInt(el_Ram.get(3).getText());
            D_Catapult_L = Integer.parseInt(el_Catapult.get(3).getText());
            D_Knight_L = Integer.parseInt(el_Knight.get(3).getText());
            D_Snob_L = Integer.parseInt(el_Snob.get(3).getText());
        }

        if(el_Spear.size() > 4) {
            D_Spear_O = Integer.parseInt(el_Spear.get(4).getText());
            D_Sword_O = Integer.parseInt(el_Sword.get(4).getText());
            D_Axe_O = Integer.parseInt(el_Axe.get(4).getText());
            D_Bow_O = Integer.parseInt(el_Bow.get(4).getText());
            D_Spy_O = Integer.parseInt(el_Spy.get(4).getText());
            D_Light_O = Integer.parseInt(el_Light.get(4).getText());
            D_MArcher_O = Integer.parseInt(el_MArcher.get(4).getText());
            D_Heavy_O = Integer.parseInt(el_Heavy.get(4).getText());
            D_Ram_O = Integer.parseInt(el_Ram.get(4).getText());
            D_Catapult_O = Integer.parseInt(el_Catapult.get(4).getText());
            D_Knight_O = Integer.parseInt(el_Knight.get(4).getText());
            D_Snob_O = Integer.parseInt(el_Snob.get(4).getText());
        }


        SaveReport();
    }
}
