package com.spb.frame.Bot;

import com.spb.frame.SmartBlock;
import com.spb.frame.SpbFile;
import com.spb.frame.TribalWars.TWResRequest;

import java.util.LinkedList;

/**
 * Created by ares7 on 19.02.2017.
 */
public class BotReports {
    public LinkedList<Integer> reportIDs = new LinkedList<>();
    public LinkedList<TWResRequest> reportsCache = new LinkedList<>();


    public BotReports() {
        loadReports();
    }

    public void saveReports() {
        SmartBlock sb = new SmartBlock("Reports");
        sb.addProperty("count", reportIDs.size());
        int a = 0;
        for(Integer i : reportIDs) {
            sb.addProperty("array-" + a, i);
            a ++;
        }

        SpbFile.write("reports.sb", sb.process(), false);
    }

    public void loadReports() {
        if(!SpbFile.exists("reports.sb"))
            return;

        SmartBlock sb = new SmartBlock("Reports");
        String reports_sb = SpbFile.read("reports.sb");
        sb.process(reports_sb);

        int count = sb.getIntProperty("count");
        for(int i = 0; i < count; i ++) {
            reportIDs.add(sb.getIntProperty("array-" + i));
        }
        System.out.println("BotReports: Added " + count + " reports");
    }
}
