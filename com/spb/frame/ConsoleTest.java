package com.spb.frame;

import com.spb.frame.AccCreator.AccCreate;
import com.spb.frame.Bot.SpBot;
import com.spb.frame.SpbJS.SpbJS;
import com.spb.frame.TribalWars.TWReport;
import com.spb.frame.TribalWars.TWVillage;
import com.spb.frame.TribalWars.TribalWars;
import com.spb.frame.TribalWars.Village;
import com.spb.frame.Vars.Arguments;
import org.openqa.selenium.JavascriptExecutor;

import java.util.Scanner;

/**
 * Created by ares7 on 18.02.2017.
 */
public class ConsoleTest {
    static Spb spb;
    static Thread thread;
    static Scanner scanner;
    static SpBot spBot;
    static Arguments arguments;


    public static void main(String[] args) {
        arguments = new Arguments(args);
        spb = new Spb(TribalWars.domain);
        thread = new Thread(spb);
        thread.start();
        scanner = new Scanner(System.in);
        WaitForCommand();
    }

    static SpbJS js;
    public static void WaitForCommand() {
        String input = scanner.nextLine();

        try {
            switch (input.toLowerCase()) {
                case "jstest":
                    if(js == null) js = new SpbJS(spb.tw, spBot);
                    Object res = js.Execute(scanner.nextLine());
                    System.out.println(res);
                    break;
                case "jsrun":
                    if(js == null) js = new SpbJS(spb.tw, spBot);
                    System.out.println(js.RunScript(scanner.next()));
                    break;
                case "genacc":
                    AccCreate ac = new AccCreate(spb.brw);
                    ac.GenerateAccount();
                    break;
                case "spbot":
                    switch (scanner.next()) {
                        case "start":
                            spBot = new SpBot(spb.tw);
                            spBot.spbJS = new SpbJS(spb.tw, spBot);
                            Thread t = new Thread(spBot);
                            t.start();
                            break;
                        case "off":
                            spBot.enabled = false;
                            System.out.println("SpBot is off!");
                            break;
                        case "on":
                            spBot.enabled = true;
                            System.out.println("SpBot is on!");
                            break;
                    }
                    break;
                case "goto":
                    System.out.print("\nEnter URL with http/https: ");
                    spb.brw.navigate().to(scanner.next());
                    break;
                case "info":
                    for(TWVillage village : spb.tw.resources.villages) {
                        System.out.println("========= Village " + village.name + " (" + village.coords_x + "|" + village.coords_y + ") #" + village.villageID + "/" + village.villageHash + " =========\n"
                                + "Points: " + village.points + "\n"
                                + "Wood: " + village.R_wood + " (+" + village.P_wood + "/h), Stone: " + village.R_stone + " (+" + village.P_stone + "/h), Iron: " + village.R_iron + "(+" + village.P_iron + "/h)\n"
                                + "Storage: " + village.R_storage + ", Population: " + village.R_population + "/" + village.R_max_population + "\n\n"
                                + "[=== BUILDINGS ===]\n"
                                + "HQ: " + village.B_hq + "\t\t"
                                + "Barracks: " + village.B_barracks + "\t\t"
                                + "Church: " + village.B_churchF + "/" + village.B_church + "\n"
                                + "Place: " + village.B_place + "\t"
                                + "Wood: " + village.B_wood + "\t\t\t"
                                + "Stone: " + village.B_stone + "\n"
                                + "Iron: " + village.B_iron + "\t\t"
                                + "Storage: " + village.B_storage + "\t\t"
                                + "Farm: " + village.B_farm + "\n"
                                + "Hide: " + village.B_hide + "\t\t"
                                + "Wall: " + village.B_wall + "\t\t\t"
                                + "Statue: " + village.B_statue + "\n"
                                + "Stable: " + village.B_stable + "\t"
                                + "Garage: " + village.B_garage + "\t\t"
                                + "Watch Tower: " + village.B_watchtower + "\n"
                                + "Snob: " + village.B_snob + "\t\t"
                                + "Smith: " + village.B_smith + "\t\t"
                                + "Market: " + village.B_market + "\n"
                                + "[=== UNITS ===]\n"
                                + "Spear: " + village.U_spear + "\t\tSpy: " + village.U_spy + "\t\t\tRam: " + village.U_ram + "\t\t\tKnight: " + village.U_paladin + "\n"
                                + "Sword: " + village.U_sword + "\t\tL Cav: " + village.U_lcavalry + "\t\tCatapult: " + village.U_catapult + "\t\tNobleman: " + village.U_noble + "\n"
                                + "Axe: " + village.U_axe + "\t\t\tB Cav: " + village.U_bcavalry + "\n"
                                + "Archer: " + village.U_bow + "\t\tH Cav: " + village.U_hcavalry);
                    }
                    break;
                case "move":
                    System.out.println("In which village do you want to recruit? ");
                    int mi = 0;
                    for(TWVillage village :spb.tw.resources.villages) {
                        System.out.println("[" + mi + "] " + village.name + " (" + village.villageHash + ")");
                        mi ++;
                    }
                    System.out.print("So...? \n\n>>>> ");
                    int mvil = scanner.nextInt();

                    System.out.print("Attack/Support?\n[1] Attack\n[2] Support\n>>>> ");
                    TWVillage.MovementType movementType;
                    if(scanner.nextInt() == 1) {
                        movementType = TWVillage.MovementType.Attack;
                    } else {
                        movementType = TWVillage.MovementType.Support;
                    }

                    System.out.print("\nNow enter your troops...\nSpear: ");
                    int m_spear = scanner.nextInt();
                    System.out.print("\nSword: ");
                    int m_sword = scanner.nextInt();
                    System.out.print("\nAxe: ");
                    int m_axe = scanner.nextInt();
                    System.out.print("\nArcher: ");
                    int m_archer = scanner.nextInt();
                    System.out.print("\nSpy: ");
                    int m_spy = scanner.nextInt();
                    System.out.print("\nLight Cavalry: ");
                    int m_light = scanner.nextInt();
                    System.out.print("\nArcher Cavalry: ");
                    int m_marcher = scanner.nextInt();
                    System.out.print("\nHeavy Cavalry: ");
                    int m_heavy = scanner.nextInt();
                    System.out.print("\nRam: ");
                    int m_ram = scanner.nextInt();
                    System.out.print("\nCatapult: ");
                    int m_catapult = scanner.nextInt();
                    System.out.print("\nKnight: ");
                    int m_knight = scanner.nextInt();
                    System.out.print("\nSnob: ");
                    int m_snob = scanner.nextInt();
                    System.out.print("\nGreat! Now target village.\nTarget X: ");
                    int m_x = scanner.nextInt();
                    System.out.print("\nTarget Y: ");
                    int m_y = scanner.nextInt();

                    TWVillage mv = spb.tw.resources.villages.get(mvil);
                    Village target = new Village(m_x, m_y, 0, "", 0);


                    mv.MoveUnits(movementType, target, m_spear, m_sword, m_axe, m_archer, m_spy, m_light, m_marcher, m_heavy, m_ram, m_catapult, m_knight, m_snob, true);
                    break;
                case "recruit":
                    System.out.println("In which village do you want to recruit? ");
                    int ri = 0;
                    for(TWVillage village :spb.tw.resources.villages) {
                        System.out.println("[" + ri + "] " + village.name + " (" + village.villageHash + ")");
                        ri ++;
                    }
                    System.out.print("So...? \n\n>>>> ");
                    int rvil = scanner.nextInt();

                    System.out.print("\nWhat do you want to recruit?\n"
                        + ">> spear\t\t>> spy\t\t>> ram\t\t>> knight\n"
                        + ">> sword\t\t>> light\t\t>> catapult\t\t>> snob\n"
                        + ">> axe\t\t>> marcher\t\t>> archer\t\t>> heavy\n\n>>>> ");
                    String r = scanner.next();

                    System.out.print("\nAnd how many? \t\t>>> ");
                    int rC = scanner.nextInt();

                    TWVillage rrv = spb.tw.resources.villages.get(rvil);
                    System.out.println("\nRecruiting " + rC + "x " + r + " in " + rrv.name + " (" + rrv.villageHash + ")...");
                    rrv.RecruitUnit(r, rC, true);
                    break;
                case "report":
                    System.out.println("Enter report ID: ");
                    spb.brw.navigate().to("https://" + TribalWars.world + "." + TribalWars.domain + "/game.php?screen=report&view=" + scanner.nextInt());
                    Utils.waitForLoad(spb.brw);
                    TWReport rep = new TWReport(spb.brw);
                    break;
                case "build":
                    System.out.println("In which village do you want to build? ");
                    int i = 0;
                    for(TWVillage village :spb.tw.resources.villages) {
                        System.out.println("[" + i + "] " + village.name + " (" + village.villageHash + ")");
                        i ++;
                    }
                    System.out.print("So...? \n\n>>>> ");
                    int vil = scanner.nextInt();

                    System.out.print("\nWhat do you want to build?\n>> main\t\t>> barracks\t\t>> church\t\t>> farm\n"
                        + ">> church_f\t\t>> place\t\t>> wood\t\t>> stone\t\t>> iron\n"
                        + ">> storage\t\t>> hide\t\t>> stable\t\t>> garage\t\t>> watchtower\n"
                        + ">> snob\t\t>> smith\t\t>> market\t\t>> wall\t\t>> statue\n\n>>>> ");
                    String b = scanner.next();

                    TWVillage rv = spb.tw.resources.villages.get(vil);
                    System.out.println("\nBuilding " + b + " in " + rv.name + " (" + rv.villageHash + ")...");
                    rv.UpgradeBuilding(b, true);
                    break;
                case "research":
                    System.out.println("In which village do you want to research? ");
                    int rsi = 0;
                    for(TWVillage village :spb.tw.resources.villages) {
                        System.out.println("[" + rsi + "] " + village.name + " (" + village.villageHash + ")");
                        rsi ++;
                    }
                    System.out.print("So...? \n\n>>>> ");
                    int rsvil = scanner.nextInt();


                    System.out.print("\nWhat do you want to research?\n>> spear\t\t>> sword\t\t>> axe\t\t>> archer\n"
                            + ">> spy\t\t>> light\t\t>> marcher\t\t>> heavy\t\t>> ram\n"
                            + ">> catapult\n\n>>>> ");
                    String rsb = scanner.next();

                    TWVillage rsv = spb.tw.resources.villages.get(rsvil);
                    System.out.println("\nResearching " + rsb + " in " + rsv.name + " (" + rsv.villageHash + ")...");
                    rsv.ResearchUnit(rsb, true);
                    break;
                case "exe":
                    System.out.print("\nExecute JavaScript:\n");
                    String a = scanner.next();
                    String out = ((JavascriptExecutor) spb.brw).executeScript(a).toString();
                    System.out.print("\n\n>> JS Output:\n" + out);
                    break;
                case "trade_send":
                    System.out.println("In which village do you want to trade? ");
                    int tsi = 0;
                    for(TWVillage village :spb.tw.resources.villages) {
                        System.out.println("[" + tsi + "] " + village.name + " (" + village.villageHash + ")");
                        tsi ++;
                    }
                    System.out.print("So...? \n\n>>>> ");
                    int tsvil = scanner.nextInt();
                    TWVillage tsv = spb.tw.resources.villages.get(tsvil);

                    System.out.println("\nSend to (xxx|yyy): ");
                    String coords = scanner.next();

                    System.out.println("Wood: ");
                    int wood = scanner.nextInt();
                    System.out.println("Stone: ");
                    int stone = scanner.nextInt();
                    System.out.println("Iron: ");
                    int iron = scanner.nextInt();

                    tsv.market.Send(new Village(Integer.parseInt(coords.split("\\|")[0]), Integer.parseInt(coords.split("\\|")[1]), 0, "", 0), wood, stone, iron, true);
                    break;
                case "trade_offer":
                    System.out.println("In which village do you want to trade? ");
                    int toi = 0;
                    for(TWVillage village :spb.tw.resources.villages) {
                        System.out.println("[" + toi + "] " + village.name + " (" + village.villageHash + ")");
                        toi ++;
                    }
                    System.out.print("So...? \n\n>>>> ");
                    int tovil = scanner.nextInt();
                    TWVillage tov = spb.tw.resources.villages.get(tovil);

                    System.out.println("What do you offer? Wood/Stone/Iron ");
                    TWVillage.Market.Resource offeringResource = null;
                    switch (scanner.next().toLowerCase()) {
                        case "wood":
                            offeringResource = TWVillage.Market.Resource.Wood;
                            break;
                        case "stone":
                            offeringResource = TWVillage.Market.Resource.Stone;
                            break;
                        case "iron":
                            offeringResource = TWVillage.Market.Resource.Iron;
                            break;
                        default:
                            System.err.println("Unknown resource");
                            break;
                    }

                    System.out.println("How much? ");
                    int offering = scanner.nextInt();

                    System.out.println("What do you want? Wood/Stone/Iron");
                    TWVillage.Market.Resource requestingResource = null;
                    switch (scanner.next().toLowerCase()) {
                        case "wood":
                            requestingResource = TWVillage.Market.Resource.Wood;
                            break;
                        case "stone":
                            requestingResource = TWVillage.Market.Resource.Stone;
                            break;
                        case "iron":
                            requestingResource = TWVillage.Market.Resource.Iron;
                            break;
                        default:
                            System.err.println("Unknown resource");
                            break;
                    }

                    System.out.println("How much? ");
                    int requesting = scanner.nextInt();

                    tov.market.Offer(offeringResource, offering, requestingResource, requesting, true);
                    break;
                case "trade_lookfor":
                    System.out.println("In which village do you want to trade? ");
                    int tlfi = 0;
                    for(TWVillage village :spb.tw.resources.villages) {
                        System.out.println("[" + tlfi + "] " + village.name + " (" + village.villageHash + ")");
                        tlfi ++;
                    }
                    System.out.print("So...? \n\n>>>> ");
                    int tlfvil = scanner.nextInt();
                    TWVillage tlfv = spb.tw.resources.villages.get(tlfvil);

                    System.out.println("Give me a name to scan...: ");
                    String name = scanner.next();

                    tlfv.market.FindAndAcceptOffers(name, true);
                    break;
                default:
                    System.out.println("Unknown console input: " + input);
                    break;
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        WaitForCommand();
    }
}
