package com.spb.frame.Vars;

import com.spb.frame.TribalWars.TribalWars;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * Created by ares7 on 14.03.2017.
 */
public class BuildingQueueVar {
    public String name;
    public TWTime time;
    public int level;
    public String other;

    public BuildingQueueVar(WebDriver driver, WebElement element) {
        List<WebElement> data = element.findElements(By.tagName("td"));

        for(int i = 0; i < data.size(); i ++) {
            String html;
            switch (i) {
                case 0:
                    html = data.get(0).getAttribute("innerHTML");
                    String[] parts = html.split("<br>");
                    System.out.println(parts[0].split(">")[1].replace("\t", "").replace("\n", ""));
                    name = Language.getIdentifier(parts[0].split(">")[1].replace("\t", "").replace("\n", ""));
                    switch (TribalWars.self.domain.split("\\.")[TribalWars.self.domain.split("\\.").length - 1]) {
                        case "cz":
                            level = Integer.parseInt(parts[1].split(" ")[1].replace("\t", ""));
                            break;
                    }
                    break;
                case 1:
                    time = new TWTime(driver, data.get(1).getText());
                    break;
                case 3:
                    other = data.get(0).getText();
                    break;
            }
        }
    }
}
