package com.spb.frame.Bot;

/**
 * Created by ares7 on 19.02.2017.
 */
public class VillageRecruitingQueue {
    public long RecruitDeadline = 0;
    public String unit = "";
    public int count = 0;
    public int recruited = 0;
    public BotVillage.QueueUrgency urgency = null;

    public VillageRecruitingQueue(String unit, int count) {
        this.unit = unit;
        this.count = count;
    }

    public void addDeadline(long time) {
        RecruitDeadline = time;
    }
}
