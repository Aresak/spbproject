package com.spb.frame.Vars;

import com.spb.frame.TribalWars.TribalWars;

/**
 * Created by ares7 on 16.03.2017.
 */
public class Language {

    public static String getIdentifier(String input) {
        String out = "";
        switch (TribalWars.self.domain.split("\\.")[TribalWars.self.domain.split("\\.").length - 1]) {
            case "cz":
                switch (input.toLowerCase()) {
                    case "hlavní budova":
                        out = "main";
                        break;
                    case "kasárna":
                        out = "barracks";
                        break;
                    case "první kostel":
                        out = "church_f";
                        break;
                    case "kovárna":
                        out = "smith";
                        break;
                    case "nádvoří":
                        out = "place";
                        break;
                    case "tržiště":
                        out = "market";
                        break;
                    case "dřevorubec":
                        out = "wood";
                        break;
                    case "lom na těžbu hlíny":
                        out = "stone";
                        break;
                    case "železný důl":
                        out = "iron";
                        break;
                    case "selský dvůr":
                        out = "farm";
                        break;
                    case "skladiště":
                        out = "storage";
                        break;
                    case "skrýš":
                        out = "hide";
                        break;
                    case "hradby":
                        out = "wall";
                        break;
                    case "stáj":
                        out = "stable";
                        break;
                    case "dílna":
                        out = "garage";
                        break;
                    case "šlechtic":
                    case "panský dvůr":
                        out = "snob";
                        break;
                    case "strážní věž":
                        out = "watch_tower";
                        break;
                    case "kopiník":
                        out = "spear";
                        break;
                    case "šermíř":
                        out = "sword";
                        break;
                    case "sekerník":
                        out = "axe";
                        break;
                    case "lučištník":
                        out = "archer";
                        break;
                    case "lehká kavalérie":
                        out = "lcavalry";
                        break;
                    case "lučištník na koni":
                        out = "bcavalry";
                        break;
                    case "těžká kavalérie":
                        out = "hcavalary";
                        break;
                    case "beranidlo":
                        out = "ram";
                        break;
                    case "katapult":
                        out = "catapult";
                        break;
                    default:
                        System.err.println("Couldn't find language label for: " + input.toLowerCase());
                        break;
                }
                break;
            default:
                System.err.println("Unknown language server!");
                break;
        }
        return out;
    }

    public static String getLocal(String input) {
        String out = "";
        switch (TribalWars.self.domain.split(".")[TribalWars.self.domain.split(".").length - 1]) {
            case "cz":
                switch (input.toLowerCase()) {
                    case "level":
                        out = "Stupeň";
                        break;
                }
            default:
                System.err.println("Unknown language server!");
                break;
        }
        return out;
    }
}
