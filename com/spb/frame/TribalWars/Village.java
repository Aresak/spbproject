package com.spb.frame.TribalWars;

/**
 * Created by ares7 on 19.02.2017.
 */
public class Village {
    public int coords_x = 0, coords_y = 0;
    public String name = "";
    public int id = 0;
    public int point = 0;

    public Village(int x, int y, int id, String name, int points) {
        this.coords_x = x;
        this.coords_y = y;
        this.id = id;
        this.name = name;
        this.point = points;
    }
}
