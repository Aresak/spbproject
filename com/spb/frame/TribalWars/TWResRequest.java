package com.spb.frame.TribalWars;

/**
 * Created by ares7 on 19.02.2017.
 */
public class TWResRequest {
    public int wood = 0;
    public int stone = 0;
    public int iron = 0;
    public int population = 0;
    public long time = 0;
}
