package com.spb.frame;

import autoitx4java.AutoItX;
import com.jacob.com.LibraryLoader;
import com.spb.frame.AccCreator.AccCreate;
import com.spb.frame.TribalWars.TribalWars;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * Created by ares7 on 18.02.2017.
 */
public class Spb implements Runnable {
    public final String bwi_class = "Chrome_WidgetWin_1";
    public boolean active = false;
    public AutoItX aix;
    public WebDriver brw;


    public TribalWars tw;



    private String domain;
    public Spb(String domain) {
        String jacobDllVersionToUse;
        if (System.getProperty("sun.arch.data.model").contains("32")){
            jacobDllVersionToUse = "jacob-1.18-x86.dll";
        }
        else {
            jacobDllVersionToUse = "jacob-1.18-x64.dll";
        }

        File file = new File("lib", jacobDllVersionToUse);
        System.setProperty(LibraryLoader.JACOB_DLL_PATH, file.getAbsolutePath());
        this.domain = domain;
    }

    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {
        File file = new File("tools", "geckodriver.exe");
        System.setProperty("webdriver.gecko.driver", file.getAbsolutePath());

        active = true;

        aix = new AutoItX();

        File torProfileDir = new File(
                "C:\\data\\Spb\\Tor\\Browser\\TorBrowser\\Data\\Browser\\profile.default");
        FirefoxBinary binary = new FirefoxBinary(new File(
                "C:\\data\\Spb\\Tor\\Browser\\firefox.exe"));
        FirefoxProfile torProfile = new FirefoxProfile(torProfileDir);
        torProfile.setPreference("webdriver.load.strategy", "unstable");
        torProfile.setPreference("javascript.enabled", true);
        try {
            binary.startProfile(torProfile, torProfileDir, "");
        } catch (IOException e) {
            e.printStackTrace();
        }


        FirefoxProfile profile = new FirefoxProfile();
        profile.setPreference("network.proxy.type", 1);
        profile.setPreference("network.proxy.socks", "127.0.0.1");
        profile.setPreference("network.proxy.socks_port", 9150);


        brw = new FirefoxDriver(profile);
        brw.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        brw.get("https://" + domain);
        Utils.waitForLoad(brw);

        //AccCreate ac = new AccCreate(brw);
        //ac.GenerateAccount();
        tw = new TribalWars(brw);
    }
}
