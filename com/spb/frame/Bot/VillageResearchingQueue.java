package com.spb.frame.Bot;

/**
 * Created by ares7 on 19.02.2017.
 */
public class VillageResearchingQueue {
    public long Deadline = 0;
    public String research = "";
    public BotVillage.QueueUrgency urgency = null;


    public VillageResearchingQueue(String research) {
        this.research = research;
    }

    public void addDeadline(long time) {
        Deadline = time;
    }
}
