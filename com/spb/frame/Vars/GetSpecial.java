package com.spb.frame.Vars;

/**
 * Created by ares7 on 17.03.2017.
 */
public class GetSpecial {
    public static int Traders(int level) {
        switch (level) {
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
                return 3;
            case 4:
                return 4;
            case 5:
                return 5;
            case 6:
                return 6;
            case 7:
                return 7;
            case 8:
                return 8;
            case 9:
                return 9;
            case 10:
                return 10;
            case 11:
                return 11;
            case 12:
                return 14;
            case 13:
                return 19;
            case 14:
                return 26;
            case 15:
                return 35;
            case 16:
                return 46;
            case 17:
                return 59;
            case 18:
                return 74;
            case 19:
                return 91;
            case 20:
                return 110;
            case 21:
                return 131;
            case 22:
                return 154;
            case 23:
                return 179;
            case 24:
                return 206;
            case 25:
                return 235;
            default:
                return 0;
        }
    }
}
