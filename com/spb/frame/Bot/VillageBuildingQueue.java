package com.spb.frame.Bot;

/**
 * Created by ares7 on 19.02.2017.
 */
public class VillageBuildingQueue {
    public long Deadline = 0;
    public String building = "";
    public int requestedLevel = 0;
    public BotVillage.QueueUrgency urgency = null;

    public VillageBuildingQueue(String building, int requestedLevel) {
        this.building = building;
        this.requestedLevel = requestedLevel;
    }

    public void addDeadline(long time) {
        Deadline = time;
    }
}
