package com.spb.frame.SpbJS;

import com.spb.frame.Bot.SpBot;
import com.spb.frame.SmartBlock;
import com.spb.frame.SpbFile;
import com.spb.frame.TribalWars.TribalWars;
import com.spb.frame.Utils;

import javax.script.Bindings;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.util.Dictionary;
import java.util.Hashtable;

/**
 * Created by ares7 on 22.02.2017.
 */
public class SpbJS {
    public TribalWars tribalWars;
    public SpBot spBot;
    public File file;


    ScriptEngineManager manager;
    ScriptEngine engine;
    static SpbJSScript script_owner;


    public SpbJS(TribalWars tw, SpBot spbot) {
        tribalWars = tw;
        spBot = spbot;
        manager = new ScriptEngineManager ();
        engine = manager.getEngineByName ("JavaScript");

        file = new File();
    }

    public Object Execute(String script) {
        try {
            String[] scripts = script.split("spbjs;");
            engine.put("tw", tribalWars);
            engine.put("bot", spBot);
            engine.put("script", this);
            engine.put("web", tribalWars.driver);
            engine.put("TW_SCRIPT_BEGIN", false);
            if(scripts.length == 2) {
                script_owner = new SpbJSScript((Bindings)engine.eval(scripts[0] + "spbjs;"), scripts[1]);

                System.out.println(engine.get("spbjs"));

                Object res = engine.eval (scripts[1]);

                script_owner = null;
                return res;
            }
            else
                return engine.eval (script);
        } catch (ScriptException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Object RunScript(String path) {
        if(!SpbFile.exists("Scripts\\" + path + ".spbjs"))
            return null;

        String script = SpbFile.read("Scripts\\" + path + ".spbjs");
        return Execute(script);
    }


    public void Goto(String url) {
        Utils.waitForLoad(tribalWars.driver);
        tribalWars.driver.navigate().to(url);
        Utils.waitForLoad(tribalWars.driver);
    }

    public class File {
        public boolean isExist(String file) {
            if(script_owner != null) {
                System.out.println("SpbJS [" + script_owner.name + "]: Checking file " + file);
            }
            return SpbFile.exists(file);
        }

        public String read(String file) {
            if(script_owner != null) {
                System.out.println("SpbJS [" + script_owner.name + "]: Reads file " + file);
            }
            return SpbFile.read(file);
        }

        public void write(String file, String content, boolean append) {
            if(script_owner != null) {
                System.out.println("SpbJS [" + script_owner.name + "]: Writes file " + file + " with append " + append);
            }
            SpbFile.write(file, content, append);
        }

        public void delete(String file) {
            if(script_owner != null) {
                System.out.println("SpbJS [" + script_owner.name + "]: Deletes file " + file);
            }
            SpbFile.delete(file);
        }
    }

    public class Global {
        public Dictionary<String, Object> variables = new Hashtable<>();

        public void Set(String variable, Object object) {
            variables.put(variable, object);
        }

        public Object Get(String variable) {
            return variables.get(variable);
        }
    }
}
