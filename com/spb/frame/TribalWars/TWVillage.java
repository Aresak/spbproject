package com.spb.frame.TribalWars;

import com.spb.frame.SmartBlock;
import com.spb.frame.SpbFile;
import com.spb.frame.Utils;
import com.spb.frame.Vars.BuildingQueueVar;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by ares7 on 18.02.2017.
 */
public class TWVillage {

    public TWResources _parent;
    public TWVillage(int x, int y, int id, String name, boolean deep, TWResources parent) {
        coords_x = x;
        coords_y = y;
        this.name = name;
        _parent = parent;
        villageID = id;
        this.deep = deep;

        villageHash = Integer.parseInt("" + x + "" + y);
    }

    public void UpdateLow() {
        // Update what's seen in overview

    }

    public void UpdateDeep() {
        // Update everything
        WebDriver d = _parent._parent.driver;
        JSUpdate();
        U_spear = Integer.parseInt(d.findElement(By.id("unit_input_spear")).getAttribute("data-all-count"));
        U_sword = Integer.parseInt(d.findElement(By.id("unit_input_sword")).getAttribute("data-all-count"));
        U_axe = Integer.parseInt(d.findElement(By.id("unit_input_axe")).getAttribute("data-all-count"));
        if(Utils.isElementPresent(d, By.id("unit_input_archer")))
            U_bow = Integer.parseInt(d.findElement(By.id("unit_input_archer")).getAttribute("data-all-count"));
        else
            U_bow = 0;
        U_spy = Integer.parseInt(d.findElement(By.id("unit_input_spy")).getAttribute("data-all-count"));
        U_lcavalry = Integer.parseInt(d.findElement(By.id("unit_input_light")).getAttribute("data-all-count"));
        if(Utils.isElementPresent(d, By.id("unit_input_marcher")))
            U_bcavalry = Integer.parseInt(d.findElement(By.id("unit_input_marcher")).getAttribute("data-all-count"));
        else
            U_bcavalry = 0;
        U_hcavalry = Integer.parseInt(d.findElement(By.id("unit_input_heavy")).getAttribute("data-all-count"));
        U_ram = Integer.parseInt(d.findElement(By.id("unit_input_ram")).getAttribute("data-all-count"));
        U_catapult = Integer.parseInt(d.findElement(By.id("unit_input_catapult")).getAttribute("data-all-count"));
        U_noble = Integer.parseInt(d.findElement(By.id("unit_input_snob")).getAttribute("data-all-count"));

        if(Utils.isElementPresent(d, By.id("unit_input_knight")))
            U_paladin = Integer.parseInt(d.findElement(By.id("unit_input_knight")).getAttribute("data-all-count"));
        else
            U_paladin = 0;
    }

    public void JSUpdate() {
        WebDriver d = _parent._parent.driver;
        JavascriptExecutor js = (JavascriptExecutor) d;


        points = Integer.parseInt(js.executeScript("return game_data.village.points").toString());

        R_wood = Integer.parseInt(js.executeScript("return game_data.village.wood").toString());
        R_stone = Integer.parseInt(js.executeScript("return game_data.village.stone").toString());
        R_iron = Integer.parseInt(js.executeScript("return game_data.village.iron").toString());
        R_storage = Integer.parseInt(js.executeScript("return game_data.village.storage_max").toString());
        R_population = Integer.parseInt(js.executeScript("return game_data.village.pop").toString());
        R_max_population = Integer.parseInt(js.executeScript("return game_data.village.pop_max").toString());


        P_wood = (int) Math.round(Double.parseDouble(js.executeScript("return game_data.village.wood_prod").toString()) * 60 * 60);
        P_stone = (int) Math.round(Double.parseDouble(js.executeScript("return game_data.village.stone_prod").toString()) * 60 * 60);
        P_iron = (int) Math.round(Double.parseDouble(js.executeScript("return game_data.village.iron_prod").toString()) * 60 * 60);

        B_hq = Integer.parseInt(js.executeScript("return game_data.village.buildings.main").toString());
        B_barracks = Integer.parseInt(js.executeScript("return game_data.village.buildings.barracks").toString());
        B_church = Integer.parseInt(js.executeScript("return game_data.village.buildings.church").toString());
        B_churchF = Integer.parseInt(js.executeScript("return game_data.village.buildings.church_f").toString());
        B_place = Integer.parseInt(js.executeScript("return game_data.village.buildings.place").toString());
        B_wood = Integer.parseInt(js.executeScript("return game_data.village.buildings.wood").toString());
        B_stone = Integer.parseInt(js.executeScript("return game_data.village.buildings.stone").toString());
        B_iron = Integer.parseInt(js.executeScript("return game_data.village.buildings.iron").toString());
        B_farm = Integer.parseInt(js.executeScript("return game_data.village.buildings.farm").toString());
        B_storage = Integer.parseInt(js.executeScript("return game_data.village.buildings.storage").toString());
        B_hide = Integer.parseInt(js.executeScript("return game_data.village.buildings.hide").toString());
        B_stable = Integer.parseInt(js.executeScript("return game_data.village.buildings.stable").toString());
        B_garage = Integer.parseInt(js.executeScript("return game_data.village.buildings.garage").toString());
        B_watchtower = Integer.parseInt(js.executeScript("return game_data.village.buildings.watchtower").toString());
        B_snob = Integer.parseInt(js.executeScript("return game_data.village.buildings.snob").toString());
        B_smith = Integer.parseInt(js.executeScript("return game_data.village.buildings.smith").toString());
        B_market = Integer.parseInt(js.executeScript("return game_data.village.buildings.market").toString());
        B_wall = Integer.parseInt(js.executeScript("return game_data.village.buildings.wall").toString());
        B_statue = Integer.parseInt(js.executeScript("return game_data.village.buildings.statue").toString());
    }

    public void SaveVillage() {
        SmartBlock village = new SmartBlock("Village");
        village.addProperty("coords_x", coords_x);
        village.addProperty("coords_y", coords_y);
        village.addProperty("name", name);
        village.addProperty("hash", villageHash);
        village.addProperty("deep_update", deep);
        village.addProperty("id", villageID);
        village.addProperty("R_wood", R_wood);
        village.addProperty("R_stone", R_stone);
        village.addProperty("R_iron", R_iron);
        village.addProperty("R_population", R_population);
        village.addProperty("R_max_population", R_max_population);
        village.addProperty("R_storage", R_storage);
        village.addProperty("P_wood", P_wood);
        village.addProperty("P_stone", P_stone);
        village.addProperty("P_iron", P_iron);
        village.addProperty("points", points);
        village.addProperty("B_hq", B_hq);
        village.addProperty("B_church", B_church);
        village.addProperty("B_churchF", B_churchF);
        village.addProperty("B_place", B_place);
        village.addProperty("B_wood", B_wood);
        village.addProperty("B_stone", B_stone);
        village.addProperty("B_iron", B_iron);
        village.addProperty("B_farm", B_farm);
        village.addProperty("B_storage", B_storage);
        village.addProperty("B_hide", B_hide);
        village.addProperty("B_barracks", B_barracks);
        village.addProperty("B_stable", B_stable);
        village.addProperty("B_garage", B_garage);
        village.addProperty("B_watchtower", B_watchtower);
        village.addProperty("B_snob", B_snob);
        village.addProperty("B_smith", B_smith);
        village.addProperty("B_market", B_market);
        village.addProperty("B_wall", B_wall);
        village.addProperty("B_statue", B_statue);
        village.addProperty("U_spear", U_spear);
        village.addProperty("U_sword", U_sword);
        village.addProperty("U_axe", U_axe);
        village.addProperty("U_bow", U_bow);
        village.addProperty("U_spy", U_spy);
        village.addProperty("U_lcavalry", U_lcavalry);
        village.addProperty("U_bcavalry", U_bcavalry);
        village.addProperty("U_hcavalry", U_hcavalry);
        village.addProperty("U_ram", U_ram);
        village.addProperty("U_catapult", U_catapult);
        village.addProperty("U_noble", U_noble);
        village.addProperty("U_paladin", U_paladin);

        SpbFile.write("Villages\\tw_" + villageHash + ".sb", village.process(), false);
    }

    public void LoadVillage(int hash) {
        SmartBlock village = new SmartBlock("Village");
        village.process(SpbFile.read("Villages\\tw_" + hash + ".sb"));
        coords_y = village.getIntProperty("coords_y");
        coords_x = village.getIntProperty("coords_x");
        name = village.getProperty("name");
        villageID = village.getIntProperty("id");
        villageHash = village.getIntProperty("hash");
        deep = village.getBoolProperty("deep_update");
    }

    public static TWVillage GenerateVillage(WebDriver driver, TWResources resources) {
        System.out.println("Generating new village...");
        Utils.waitForLoad(driver);
        int id = Integer.parseInt(
                Utils.GetURLParams(
                        driver.findElement(
                                By.id("menu_row2_village")
                        ).findElement(
                                By.tagName("a")
                        ).getAttribute("href")
                ).get("village"));
        String coords = driver.findElements(By.className("box-item")).get(1).getText();
        int x = Integer.parseInt(coords.substring(1,4)), y = Integer.parseInt(coords.substring(6, 8));
        String name = driver.findElement(By.id("menu_row2_village")).getText();

        TWVillage village = new TWVillage(x, y, id, name, false, resources);
        village.UpdateDeep();
        System.out.println("========= New village founded: " + name + " (" + x + "|" + y + ") #" + id + "/" + village.villageHash + " =========\n"
            + "Points: " + village.points + "\n"
            + "Wood: " + village.R_wood + " (+" + village.P_wood + "/h), Stone: " + village.R_stone + " (+" + village.P_stone + "/h), Iron: " + village.R_iron + "(+" + village.P_iron + "/h)\n"
            + "Storage: " + village.R_storage + ", Population: " + village.R_population + "/" + village.R_max_population + "\n\n"
            + "[=== BUILDINGS ===]\n"
            + "HQ: " + village.B_hq + "\t\t"
            + "Barracks: " + village.B_barracks + "\t\t"
            + "Church: " + village.B_churchF + "/" + village.B_church + "\n"
            + "Place: " + village.B_place + "\t"
            + "Wood: " + village.B_wood + "\t\t\t"
            + "Stone: " + village.B_stone + "\n"
            + "Iron: " + village.B_iron + "\t\t"
            + "Storage: " + village.B_storage + "\t\t"
            + "Farm: " + village.B_farm + "\n"
            + "Hide: " + village.B_hide + "\t\t"
            + "Wall: " + village.B_wall + "\t\t\t"
            + "Statue: " + village.B_statue + "\n"
            + "Stable: " + village.B_stable + "\t"
            + "Garage: " + village.B_garage + "\t\t"
            + "Watch Tower: " + village.B_watchtower + "\n"
            + "Snob: " + village.B_snob + "\t\t"
            + "Smith: " + village.B_smith + "\t\t"
            + "Market: " + village.B_market + "\n"
            + "[=== UNITS ===]\n"
            + "Spear: " + village.U_spear + "\t\tSpy: " + village.U_spy + "\t\t\tRam: " + village.U_ram + "\t\t\tKnight: " + village.U_paladin + "\n"
            + "Sword: " + village.U_sword + "\t\tL Cav: " + village.U_lcavalry + "\t\tCatapult: " + village.U_catapult + "\t\tNobleman: " + village.U_noble + "\n"
            + "Axe: " + village.U_axe + "\t\t\tB Cav: " + village.U_bcavalry + "\n"
            + "Archer: " + village.U_bow + "\t\tH Cav: " + village.U_hcavalry);

        village.market = new Market(village);
        village.snob = new Snob(village);
        return village;
    }

    public boolean UpgradeBuilding(String building, boolean comeback) {
        String cbURL = _parent._parent.driver.getCurrentUrl();
        WebDriver d = _parent._parent.driver;
        if(!cbURL.equals("https://" + TribalWars.world + "." + TribalWars.domain + "/game.php?village=" + villageID + "&screen=main")) {
            d.navigate().to("https://" + TribalWars.world + "." + TribalWars.domain + "/game.php?village=" + villageID + "&screen=main");
        }
        Utils.waitForLoad(d);

        LinkedList<BuildingQueueVar> queue = getCurrentBuildingQueue(false);
        if((_parent._parent.premium && queue.size() <= 5) || (!_parent._parent.premium && queue.size() <= 2)) {
            WebElement el = d.findElement(By.id("main_buildrow_" + building)).findElement(By.className("btn-build"));
            if(el.isDisplayed()) {
                el.click();
                System.out.println("Build: Applied building command for " + building + "!");
            }
            else {
                System.err.println("Failed to build " + building + ": " + d.findElement(By.id("main_buildrow_" + building)).findElement(By.className("inactive")).getText());
            }
            Utils.waitForLoad(d);

            if(Utils.isElementPresent(d, By.className("btn-instant-free"))) {
                for(WebElement free_build : d.findElements(By.className("btn-instant-free"))) {
                    if(!free_build.isDisplayed())
                        continue;

                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    free_build.click();
                    System.out.println("Build: Insta-built " + building);
                }
            }
        } else {
            System.err.println("Failed to build " + building + " as a maximum building queue has been reached!");
        }


        JSUpdate();
        if(comeback)
            _parent._parent.driver.navigate().to(cbURL);
        Utils.waitForLoad(d);
        return true;
    }

    public TWResRequest RequestBuildingReqs(String building, boolean comeback) {
        TWResRequest twrr = new TWResRequest();
        String cbURL = _parent._parent.driver.getCurrentUrl();
        WebDriver d = _parent._parent.driver;
        if(!cbURL.equals("https://" + TribalWars.world + "." + TribalWars.domain + "/game.php?village=" + villageID + "&screen=main")) {
            d.navigate().to("https://" + TribalWars.world + "." + TribalWars.domain + "/game.php?village=" + villageID + "&screen=main");
        }
        Utils.waitForLoad(d);

        List<WebElement> elements = d.findElement(By.id("main_buildrow_" + building)).findElements(By.tagName("td"));
        twrr.wood = Integer.parseInt(elements.get(1).getText());
        twrr.stone = Integer.parseInt(elements.get(2).getText());
        twrr.iron = Integer.parseInt(elements.get(3).getText());
        String[] time = elements.get(4).getText().split(":");
        twrr.population = Integer.parseInt(elements.get(5).getText());
        twrr.time = Integer.parseInt(time[2]) + (Integer.parseInt(time[1]) * 60) + (Integer.parseInt(time[0]) * 60 * 60);

        if(comeback)
            _parent._parent.driver.navigate().to(cbURL);
        Utils.waitForLoad(d);
        return twrr;
    }

    public boolean RecruitUnitWOSubmit(String unit, int count) {
        WebDriver d = _parent._parent.driver;
        String s = d.findElement(By.id(unit + "_0_a")).getText();
        if(count > Integer.parseInt(s.substring(1, s.length() - 1))) {
            System.err.println("Can't recruit this much " + unit + " as I don't have resources.");
            return false;
        }

        d.findElement(By.id(unit + "_0")).sendKeys(Integer.toString(count));
        return true;
    }

    public boolean RecruitUnit(String unit, int count, boolean comeback) {
        String cbURL = _parent._parent.driver.getCurrentUrl();
        WebDriver d = _parent._parent.driver;
        if(!cbURL.equals("https://" + TribalWars.world + "." + TribalWars.domain + "/game.php?village=" + villageID + "&screen=train")) {
            d.navigate().to("https://" + TribalWars.world + "." + TribalWars.domain + "/game.php?village=" + villageID + "&screen=train");
        }
        Utils.waitForLoad(d);

        String s = d.findElement(By.id(unit + "_0_a")).getText();
        if(count > Integer.parseInt(s.substring(1, s.length() - 1))) {
            System.err.println("Can't recruit this much " + unit + " as I don't have resources.");
            return false;
        }

        d.findElement(By.id(unit + "_0")).sendKeys(Integer.toString(count));
        d.findElement(By.className("btn-recruit")).click();
        Utils.waitForLoad(d);

        JSUpdate();
        if(comeback)
            _parent._parent.driver.navigate().to(cbURL);
        Utils.waitForLoad(d);
        return true;
    }

    public boolean ResearchUnit(String unit, boolean comeback) {
        String cbURL = _parent._parent.driver.getCurrentUrl();
        WebDriver d = _parent._parent.driver;
        if(!cbURL.equals("https://" + TribalWars.world + "." + TribalWars.domain + "/game.php?village=" + villageID + "&screen=smith")) {
            d.navigate().to("https://" + TribalWars.world + "." + TribalWars.domain + "/game.php?village=" + villageID + "&screen=smith");
        }
        Utils.waitForLoad(d);

        JavascriptExecutor js = (JavascriptExecutor) d;
        boolean b = Boolean.parseBoolean( js.executeScript("return BuildingSmith.research('" + unit + "')").toString() );
        if(b) {
            System.out.println("Research: Researched " + unit);
        }
        else {
            System.out.println("Research Failed: No resources/or requirements");
        }


        JSUpdate();
        if(comeback)
            _parent._parent.driver.navigate().to(cbURL);
        Utils.waitForLoad(d);
        return b;
    }

    public boolean MoveUnits(MovementType mt,
                             Village village,
                             int spear,
                             int sword,
                             int axe,
                             int bow,
                             int spy,
                             int lcavalry,
                             int bcavalry,
                             int hcavalry,
                             int ram,
                             int catapult,
                             int knight,
                             int noble,
                             boolean comeback) {
        String cbURL = _parent._parent.driver.getCurrentUrl();
        WebDriver d = _parent._parent.driver;
        if(!cbURL.equals("https://" + TribalWars.world + "." + TribalWars.domain + "/game.php?village=" + villageID + "&screen=place")) {
            d.navigate().to("https://" + TribalWars.world + "." + TribalWars.domain + "/game.php?village=" + villageID + "&screen=place");
        }
        Utils.waitForLoad(d);

        d.findElement(By.id("unit_input_spear")).sendKeys("" + spear);
        d.findElement(By.id("unit_input_sword")).sendKeys("" + sword);
        d.findElement(By.id("unit_input_axe")).sendKeys("" + axe);
        d.findElement(By.id("unit_input_archer")).sendKeys("" + bow);
        d.findElement(By.id("unit_input_spy")).sendKeys("" + spy);
        d.findElement(By.id("unit_input_light")).sendKeys("" + lcavalry);
        d.findElement(By.id("unit_input_marcher")).sendKeys("" + bcavalry);
        d.findElement(By.id("unit_input_heavy")).sendKeys("" + hcavalry);
        d.findElement(By.id("unit_input_ram")).sendKeys("" + ram);
        d.findElement(By.id("unit_input_catapult")).sendKeys("" + catapult);
        d.findElement(By.id("unit_input_knight")).sendKeys("" + knight);
        d.findElement(By.id("unit_input_snob")).sendKeys("" + noble);
        d.findElement(By.className("target-input-field")).sendKeys(village.coords_x + "|" + village.coords_y);

        if(mt == MovementType.Attack) {
            d.findElement(By.id("target_attack")).click();
        } else if(mt == MovementType.Support) {
            d.findElement(By.id("target_support")).click();
        }
        Utils.waitForLoad(d);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        d.findElement(By.id("troop_confirm_go")).click();

        Utils.waitForLoad(d);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        JSUpdate();
        if(comeback)
            _parent._parent.driver.navigate().to(cbURL);
        Utils.waitForLoad(d);
        return true;
    }

    public LinkedList<BuildingQueueVar> getCurrentBuildingQueue(boolean comeback) {
        LinkedList<BuildingQueueVar> buildings = new LinkedList<>();

        String cbURL = _parent._parent.driver.getCurrentUrl();
        WebDriver d = _parent._parent.driver;
        if(!cbURL.equals("https://" + TribalWars.world + "." + TribalWars.domain + "/game.php?village=" + villageID + "&screen=main")) {
            d.navigate().to("https://" + TribalWars.world + "." + TribalWars.domain + "/game.php?village=" + villageID + "&screen=main");
        }
        Utils.waitForLoad(d);


        if(Utils.isElementPresent(d, By.id("buildqueue"))) {
            List<WebElement> queueElements = d.findElement(By.id("buildqueue")).findElements(By.tagName("tr"));
            int i = 0;
            int c = 1;
            for(WebElement queueElement : queueElements) {
                if(i != 0 && i != 2) {
                    BuildingQueueVar bqv = new BuildingQueueVar(d, queueElement);
                    System.out.println(c + ") Main building queue found " + bqv.name + " upgrading to " + bqv.level);
                    c ++;
                }
                i ++;
            }
        }


        JSUpdate();
        if(comeback)
            _parent._parent.driver.navigate().to(cbURL);
        Utils.waitForLoad(d);
        return buildings;
    }


    public int coords_x = 0;
    public int coords_y = 0;
    public String name = "";
    public int villageHash = 0;
    public int villageID = 0;
    public boolean deep = false;



    public int R_wood = 0;
    public int R_stone = 0;
    public int R_iron = 0;
    public int R_population = 0;

    public int R_storage = 0;
    public int R_max_population = 0;

    public int P_wood = 0;
    public int P_stone = 0;
    public int P_iron = 0;

    public int points = 0;


    public int B_hq = 0;
    public int B_church = 0;
    public int B_churchF = 0;
    public int B_place = 0;
    public int B_wood = 0;
    public int B_stone = 0;
    public int B_iron = 0;
    public int B_farm = 0;
    public int B_storage = 0;
    public int B_hide = 0;
    public int B_barracks = 0;
    public int B_stable = 0;
    public int B_garage = 0;
    public int B_watchtower = 0;
    public int B_snob = 0;
    public int B_smith = 0;
    public int B_market = 0;
    public int B_wall = 0;
    public int B_statue = 0;

    public int U_spear = 0;
    public int U_sword = 0;
    public int U_axe = 0;
    public int U_bow = 0;
    public int U_spy = 0;
    public int U_lcavalry = 0;
    public int U_bcavalry = 0;
    public int U_hcavalry = 0;
    public int U_ram = 0;
    public int U_catapult = 0;
    public int U_noble = 0;
    public int U_paladin = 0;
    public int U_traders = 0;
    public int tradersAway = 0;

    public Market market;
    public Snob snob;

    public enum MovementType {
        Attack,
        Support,
        Trade
    }

    public static class Market {
        public TWVillage village;

        public Market(TWVillage village) {
            this.village = village;
        }

        public boolean Send(Village target, int wood, int stone, int iron, boolean comeback) {
            String cbURL = village._parent._parent.driver.getCurrentUrl();
            WebDriver d = village._parent._parent.driver;
            if(!cbURL.equals("https://" + TribalWars.world + "." + TribalWars.domain + "/game.php?village=" + village.villageID + "&screen=market&mode=send")) {
                d.navigate().to("https://" + TribalWars.world + "." + TribalWars.domain + "/game.php?village=" + village.villageID + "&screen=market&mode=send");
            }
            Utils.waitForLoad(d);


            d.findElement(By.name("wood")).sendKeys("" + wood);
            d.findElement(By.name("stone")).sendKeys("" + stone);
            d.findElement(By.name("iron")).sendKeys("" + iron);
            d.findElement(By.name("input")).sendKeys(target.coords_x + "|" + target.coords_y);
            d.findElement(By.className("btn")).click();

            village.JSUpdate();
            if(comeback)
                village._parent._parent.driver.navigate().to(cbURL);
            Utils.waitForLoad(d);
            return true;
        }

        public boolean Offer(Resource offeringResource, int countO, Resource requestingResource, int countR, boolean comeback) {
            String cbURL = village._parent._parent.driver.getCurrentUrl();
            WebDriver d = village._parent._parent.driver;
            if(!cbURL.equals("https://" + TribalWars.world + "." + TribalWars.domain + "/game.php?village=" + village.villageID + "&screen=market&mode=own_offer")) {
                d.navigate().to("https://" + TribalWars.world + "." + TribalWars.domain + "/game.php?village=" + village.villageID + "&screen=market&mode=own_offer");
            }
            Utils.waitForLoad(d);

            d.findElement(By.id("res_sell_amount")).sendKeys("" + countO);
            d.findElement(By.id("res_buy_amount")).sendKeys("" + countR);

            switch (offeringResource) {
                case Wood:
                    d.findElement(By.id("res_sell_wood")).click();
                    break;
                case Stone:
                    d.findElement(By.id("res_sell_stone")).click();
                    break;
                case Iron:
                    d.findElement(By.id("res_sell_iron")).click();
                    break;
            }

            switch (requestingResource) {
                case Wood:
                    d.findElement(By.id("res_buy_wood")).click();
                    break;
                case Stone:
                    d.findElement(By.id("res_buy_stone")).click();
                    break;
                case Iron:
                    d.findElement(By.id("res_buy_iron")).click();
                    break;
            }

            d.findElements(By.className("btn")).get(0).click();


            village.JSUpdate();
            if(comeback)
                village._parent._parent.driver.navigate().to(cbURL);
            Utils.waitForLoad(d);
            return true;
        }

        public boolean FindAndAcceptOffers(String owner, boolean comeback) {
            String cbURL = village._parent._parent.driver.getCurrentUrl();
            WebDriver d = village._parent._parent.driver;
            if(!cbURL.equals("https://" + TribalWars.world + "." + TribalWars.domain + "/game.php?village=" + village.villageID + "&screen=market")) {
                d.navigate().to("https://" + TribalWars.world + "." + TribalWars.domain + "/game.php?village=" + village.villageID + "&screen=market");
            }
            Utils.waitForLoad(d);

            WebElement offersTable = d.findElements(By.className("vis")).get(10);

            List<WebElement> offers = offersTable.findElements(By.tagName("tr"));
            for(int i = 1; i < offers.size(); i ++) {
                List<WebElement> data = offers.get(i).findElements(By.tagName("td"));
                if(data.get(2).getText().toLowerCase().equals(owner.toLowerCase())) {
                    // My guy!!!

                    data.get(6).findElement(By.className("float_right")).click();

                    System.out.println("Accepted trade offer by " + owner +
                            ". Took " + data.get(0).getText() + " " + data.get(0).findElement(By.className("icon")).getAttribute("title") + " for " +
                            data.get(1).getText() + " " + data.get(1).findElement(By.className("icon")).getAttribute("title"));
                }
            }

            village.JSUpdate();
            if(comeback)
                village._parent._parent.driver.navigate().to(cbURL);
            Utils.waitForLoad(d);
            return true;
        }

        public enum Resource {
            Wood,
            Stone,
            Iron
        }
    }

    public static class Snob {
        public TWVillage village;

        public Snob(TWVillage village) {
            this.village = village;
        }
    }
}
