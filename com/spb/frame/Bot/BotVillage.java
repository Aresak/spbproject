package com.spb.frame.Bot;

import com.spb.frame.SmartBlock;
import com.spb.frame.SpbFile;
import com.spb.frame.TribalWars.TWResources;
import com.spb.frame.TribalWars.TWVillage;

import java.util.LinkedList;

/**
 * Created by ares7 on 19.02.2017.
 */
public class BotVillage {
    public TWVillage village;

    public VillageType villageType = VillageType.Defense;

    LinkedList<VillageBuildingQueue> buildingQueues = new LinkedList<>();
    LinkedList<VillageRecruitingQueue> recruitingQueue = new LinkedList<>();
    LinkedList<VillageResearchingQueue> researchingQueue = new LinkedList<>();

    public BotVillage(TWVillage village) {
        this.village = village;
    }

    public void AddBuildingToQueue(String building, int requestedLevel) {
        buildingQueues.add(new VillageBuildingQueue(building, requestedLevel));
    }

    public void AddRecruitToQueue(String unit, int requestedCount) {
        recruitingQueue.add(new VillageRecruitingQueue(unit, requestedCount));
    }

    public void AddResearchToQueue(String research) {
        researchingQueue.add(new VillageResearchingQueue(research));
    }


    public void OnFinishedBuilding(String building, int level) {
        SaveVillage();
        System.out.println("[VillageBuildingQueue] Finished " + building + " at " + level);
    }

    public void OnFinishedRecruiting(String unit, int count) {
        System.out.println("[VillageRecruitingQueue] Finished " + count + "x " + unit);
        SaveVillage();
    }

    public void OnFinishedResearch(String research) {
        SaveVillage();
        System.out.println("[VillageResearchingQueue] Finished research of " + research);
    }


    public void SaveVillage() {
        village.SaveVillage();
        SmartBlock bvillage = new SmartBlock("BotVillage");
        bvillage.addProperty("villageType", villageType.toString());
        bvillage.addProperty("bq_count", buildingQueues.size());
        bvillage.addProperty("recq_count", recruitingQueue.size());
        bvillage.addProperty("resq_count", researchingQueue.size());

        for(int i = 0; i < buildingQueues.size(); i ++) {
            bvillage.addProperty("bq_" + i + "_deadline", buildingQueues.get(i).Deadline);
            bvillage.addProperty("bq_" + i + "_building", buildingQueues.get(i).building);
            bvillage.addProperty("bq_" + i + "_requestedLevel", buildingQueues.get(i).requestedLevel);
            bvillage.addProperty("bq_" + i + "_urgency", buildingQueues.get(i).urgency.toString());
        }

        for(int i = 0; i < recruitingQueue.size(); i ++) {
            bvillage.addProperty("recq_" + i + "_deadline", recruitingQueue.get(i).RecruitDeadline);
            bvillage.addProperty("recq_" + i + "_unit", recruitingQueue.get(i).unit);
            bvillage.addProperty("recq_" + i + "_count", recruitingQueue.get(i).count);
            bvillage.addProperty("recq_" + i + "_recruited", recruitingQueue.get(i).recruited);
            bvillage.addProperty("recq_" + i + "_urgency", recruitingQueue.get(i).urgency.toString());
        }

        for(int i = 0; i < researchingQueue.size(); i ++) {
            bvillage.addProperty("resq_" + i + "_deadline", researchingQueue.get(i).Deadline);
            bvillage.addProperty("resq_" + i + "_research", researchingQueue.get(i).research);
            bvillage.addProperty("resq_" + i + "_urgency", buildingQueues.get(i).urgency.toString());
        }

        SpbFile.write("Villages\\bot_" + village.villageHash + ".sb", bvillage.process(), false);
    }

    public static BotVillage LoadVillage(int hash, TWResources resources) {
        TWVillage village = new TWVillage(0, 0, 0, "", false, resources);
        village.LoadVillage(hash);

        BotVillage bv = new BotVillage(village);
        SmartBlock sb = new SmartBlock("BotVillage");
        sb.process(SpbFile.read("Villages\\bot_" + hash + ".sb"));

        bv.villageType = VillageType.valueOf(sb.getProperty("villageType"));
        int vbqc = sb.getIntProperty("bq_count");
        int rcbqc = sb.getIntProperty("recq_count");
        int rsbqc = sb.getIntProperty("resq_count");

        for(int i = 0; i < vbqc; i ++) {
            VillageBuildingQueue q = new VillageBuildingQueue(sb.getProperty("bq_" + i + "_building"), sb.getIntProperty("bq_" + i + "_requestedLevel"));
            q.Deadline = sb.getLongProperty("bq_" + i + "_deadline");
            q.urgency = QueueUrgency.valueOf(sb.getProperty("bq_" + i + "_urgency"));
            bv.buildingQueues.add(q);
        }

        for(int i = 0; i < rcbqc; i ++) {
            VillageRecruitingQueue q = new VillageRecruitingQueue(sb.getProperty("recq_" + i + "_unit"), sb.getIntProperty("recq_" + i + "_count"));
            q.RecruitDeadline = sb.getLongProperty("recq_" + i + "_deadline");
            q.recruited = sb.getIntProperty("recq_" + i + "_recruited");
            q.urgency = QueueUrgency.valueOf(sb.getProperty("recq_" + i + "_urgency"));
            bv.recruitingQueue.add(q);
        }

        for(int i = 0; i < rsbqc; i ++) {
            VillageResearchingQueue q = new VillageResearchingQueue(sb.getProperty("resq_" + i + "_research"));
            q.Deadline = sb.getLongProperty("resq_" + i + "_deadline");
            q.urgency = QueueUrgency.valueOf(sb.getProperty("resq_" + i + "_urgency"));
            bv.researchingQueue.add(q);
        }

        return bv;
    }


    public enum QueueUrgency {
        Minimal,
        Low,
        Medium,
        High,
        Urgent
    }

    public enum VillageType {
        Attack,
        Defense,
        Mixed,
        Mine
    }
}
