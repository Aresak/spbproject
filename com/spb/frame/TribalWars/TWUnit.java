package com.spb.frame.TribalWars;

/**
 * Created by ares7 on 19.02.2017.
 */
public class TWUnit extends TWResRequest {
    public TWResRequest Unit_Sword;
    public TWResRequest Unit_Spear;
    public TWResRequest Unit_Axe;
    public TWResRequest Unit_Bow;
    public TWResRequest Unit_Spy;
    public TWResRequest Unit_LCavalry;
    public TWResRequest Unit_BCavalry;
    public TWResRequest Unit_HCavalry;
    public TWResRequest Unit_Ram;
    public TWResRequest Unit_Catapult;
    public TWResRequest Unit_Snob;
    public TWResRequest Unit_Knight;

    public TWResRequest Smith_Sword;
    public TWResRequest Smith_Axe;
    public TWResRequest Smith_Bow;
    public TWResRequest Smith_Spy;
    public TWResRequest Smith_LCavalry;
    public TWResRequest Smith_BCavalry;
    public TWResRequest Smith_HCavalry;
    public TWResRequest Smith_Ram;
    public TWResRequest Smith_Catapult;

    public TWResRequest[] Build_Main;
    public TWResRequest[] Build_Barracks;
    public TWResRequest[] Build_Stable;
    public TWResRequest[] Build_Garage;
    public TWResRequest[] Build_Snob;
    public TWResRequest[] Build_Smith;
    public TWResRequest[] Build_Place;
    public TWResRequest[] Build_Statue;
    public TWResRequest[] Build_Wood;
    public TWResRequest[] Build_Stone;
    public TWResRequest[] Build_Iron;
    public TWResRequest[] Build_Farm;
    public TWResRequest[] Build_Storage;
    public TWResRequest[] Build_Hide;
    public TWResRequest[] Build_Wall;
    public TWResRequest[] Build_ChurchF;
    public TWResRequest[] Build_Church;

    public TWUnit() {

    }
}
