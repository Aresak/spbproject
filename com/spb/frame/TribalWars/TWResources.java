package com.spb.frame.TribalWars;

import com.spb.frame.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.LinkedList;

/**
 * Created by ares7 on 18.02.2017.
 */
public class TWResources {
    public TribalWars _parent;

    public TWResources(TribalWars parent) {
        _parent = parent;
    }

    public void Init() {
        _parent.Screen(TribalWars.Screens.Place);

        WebDriver d = _parent.driver;
        //d.findElement(By.tagName("div")).sendKeys("v");
        Utils.waitForLoad(d);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        TWVillage home = TWVillage.GenerateVillage(d, this);
        home.deep = true;
        villages.add(home);
        _parent.Screen(TribalWars.Screens.Overview);
    }

    public void UpdateResources() {
        _parent.focusedVillage = Integer.parseInt(Utils.GetURLParams(_parent.driver.getCurrentUrl()).get("village"));
        for(TWVillage village : villages) {
            if(_parent.focusedVillage == village.villageHash || village.deep) {
                village.UpdateDeep();
            }
            else {
                village.UpdateLow();
            }
        }
    }

    public void CheckVillages() {

    }


    public LinkedList<TWVillage> villages = new LinkedList<>();
}
