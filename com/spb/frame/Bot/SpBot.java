package com.spb.frame.Bot;

import com.spb.frame.SmartBlock;
import com.spb.frame.SpbFile;
import com.spb.frame.SpbJS.SpbJS;
import com.spb.frame.TribalWars.TWVillage;
import com.spb.frame.TribalWars.TribalWars;
import com.spb.frame.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by ares7 on 19.02.2017.
 */
public class SpBot implements Runnable {
    public TribalWars tribalWars;
    public boolean enabled = false;

    public Delay _CurrentDelay = Delay.Long;
    public int _DelayLong = 1000;
    public int _DelayMedium = 250;
    public int _DelayLow = 25;

    public TimerTask timerTask;
    public Timer timerScheduler;

    public LinkedList<BotVillage> villages = new LinkedList<>();
    public BotReports botReports;

    public String loopScript = "";
    public String runScript = "default_bot";

    public SpbJS spbJS;



    private int newReportCheckTicks = 10;
    private int newReportCheckTick = 0;


    public SpBot(TribalWars tw) {
        this.tribalWars = tw;
        botReports = new BotReports();

        if(SpbFile.exists("spbot.sb"))
            LoadBot();


    }

    public void SetLoopScript(String script) {
        this.loopScript = script;
    }

    public void SaveBot() {
        botReports.saveReports();

        SmartBlock bot = new SmartBlock("SpBot");
        bot.addProperty("_currentDelay", _CurrentDelay.toString());
        bot.addProperty("_delayLong", _DelayLong);
        bot.addProperty("_delayMedium", _DelayMedium);
        bot.addProperty("_delayLow", _DelayLow);
        bot.addProperty("run_script", runScript);

        bot.addProperty("villages", villages.size());
        for(int i = 0; i < villages.size(); i ++) {
            bot.addProperty("village_" + i, villages.get(i).village.villageHash);
            villages.get(i).SaveVillage();
        }

        SpbFile.write("spbot.sb", bot.process(), false);
    }

    public void LoadBot() {
        botReports.loadReports();

        SmartBlock bot = new SmartBlock("SpBot");
        bot.process(SpbFile.read("spbot.sb"));

        _CurrentDelay = Delay.valueOf(bot.getProperty("_currentDelay"));
        _DelayLong = bot.getIntProperty("_delayLong");
        _DelayMedium = bot.getIntProperty("_delayMedium");
        _DelayLow = bot.getIntProperty("_delayLow");
        int villages = bot.getIntProperty("villages");
        runScript = bot.getProperty("run_script");

        for(int i = 0; i < villages; i ++) {
            BotVillage bv = BotVillage.LoadVillage(bot.getIntProperty("village_" + i), tribalWars.resources);
            this.villages.add(bv);
        }

        spbJS.RunScript(runScript);
    }

    public void AddNewBotVillage(TWVillage twVillage) {
        BotVillage nbv = new BotVillage(twVillage);
        twVillage.UpdateDeep();
        Utils.waitForLoad(tribalWars.driver);
        villages.add(nbv);
        nbv.SaveVillage();

        System.out.println("[SpBot] Registered new village!");
    }


    boolean firsttick = true;
    boolean inTick = false;
    public void Tick() {
        if(inTick)
            return;

        inTick = true;
        if(enabled) {
            spbJS.RunScript(loopScript);


            /*
            if(newReportCheckTick == newReportCheckTicks) {
                checkNewReports();
                newReportCheckTick = 0;
            } else
                newReportCheckTick ++;


            if(tribalWars.resources.villages.size() < villages.size()) {
                for(TWVillage tv : tribalWars.resources.villages) {
                    boolean bvFound = false;
                    for(BotVillage bv : villages) {
                        if(bv.village.villageHash == tv.villageHash) {
                            bvFound = true;
                            break;
                        }
                    }

                    if(!bvFound) {
                        BotVillage nbv = new BotVillage(tv);
                        tv.UpdateDeep();
                        Utils.waitForLoad(tribalWars.driver);
                        villages.add(nbv);
                        nbv.SaveVillage();

                        System.out.println("[SpBot] Registered new village!");
                    }
                }
            }


            for(BotVillage bv : villages) {
                float popFill = (bv.village.R_population / bv.village.R_max_population) * 100;

                if(popFill > 90 && bv.village.B_farm != 30) {
                    Utils.waitForLoad(tribalWars.driver);
                    String oURL = tribalWars.driver.getCurrentUrl();
                    TWResRequest twrr = bv.village.RequestBuildingReqs("farm", false);
                    Utils.waitForLoad(tribalWars.driver);
                    if(twrr.wood < bv.village.R_wood &&
                            twrr.stone < bv.village.R_stone &&
                            twrr.iron < bv.village.R_iron) {
                        System.out.println("[SpBot] Farm is filled by more than 90% and new farm level requirements are met. Upgrading farm now.");
                        bv.village.UpgradeBuilding("farm", false);
                        Utils.waitForLoad(tribalWars.driver);
                    }
                    tribalWars.driver.navigate().to(oURL);
                    Utils.waitForLoad(tribalWars.driver);
                }

                float woodFill = (bv.village.R_wood / bv.village.R_storage) * 100;
                float stoneFill = (bv.village.R_stone / bv.village.R_storage) * 100;
                float ironFill = (bv.village.R_iron / bv.village.R_storage) * 100;

                if(woodFill >= 50 && stoneFill >= 50 && ironFill >= 50) {
                    String build = "";
                    if (bv.village.B_wood > bv.village.B_stone) {
                        if (bv.village.B_wood > bv.village.B_iron) {
                            if (bv.village.B_iron > bv.village.B_stone) {
                                // Stone lowest
                                build = "stone";
                            } else {
                                // Iron lowest
                                build = "iron";
                            }
                        } else {
                            // Stone lowest
                            build = "stone";
                        }
                    } else if (bv.village.B_stone > bv.village.B_iron) {
                        if (bv.village.B_iron > bv.village.B_wood) {
                            // Wood lowest
                            build = "wood";
                        } else {
                            // Iron lowest
                            build = "iron";
                        }
                    } else {
                        // Wood lowest
                        build = "wood";
                    }
                    Utils.waitForLoad(tribalWars.driver);
                    String oURL = tribalWars.driver.getCurrentUrl();
                    TWResRequest twrr = bv.village.RequestBuildingReqs(build, false);
                    Utils.waitForLoad(tribalWars.driver);
                    if(twrr.wood < bv.village.R_wood && twrr.stone < bv.village.R_stone && twrr.iron < bv.village.R_iron) {
                        if(twrr.population < (bv.village.R_max_population - bv.village.R_population)) {
                            bv.village.UpgradeBuilding(build, false);
                        }
                        else {
                            TWResRequest twrr2 = bv.village.RequestBuildingReqs("farm", false);
                            Utils.waitForLoad(tribalWars.driver);
                            if(twrr2.wood < bv.village.R_wood &&
                                    twrr2.stone < bv.village.R_stone &&
                                    twrr2.iron < bv.village.R_iron) {
                                // System.out.println("[SpBot] Farm is filled by more than 90% and new farm level requirements are met. Upgrading farm now.");
                                bv.village.UpgradeBuilding("farm", false);
                                // Utils.waitForLoad(tribalWars.driver);
                            }
                        }
                    }

                    Utils.waitForLoad(tribalWars.driver);
                    tribalWars.driver.navigate().to(oURL);
                    Utils.waitForLoad(tribalWars.driver);
                }
            }


            */
            if(firsttick) {
                SaveBot();
                firsttick = false;
            }
        }
        inTick = false;
    }

    public void checkNewReports() {
        JavascriptExecutor js = (JavascriptExecutor) tribalWars.driver;
        int newReports = Integer.parseInt(js.executeScript("return game_data.player.new_report").toString());
        if(newReports > 0) {
            WebDriver d = tribalWars.driver;
            d.navigate().to(TribalWars.world + "." + TribalWars.domain + "/game.php?screen=report");
            Utils.waitForLoad(d);

            List<WebElement> reports = d.findElements(By.className("quickedit"));
            for(WebElement report : reports) {
                int rid = Integer.parseInt(report.getAttribute("data-id"));
                boolean found = false;
                for(int ID : botReports.reportIDs) {
                    if(rid == ID) {
                        found = true;
                        break;
                    }
                }

                if(!found) {
                    d.navigate().to("https://" + TribalWars.world + "." + TribalWars.domain + "/game.php?screen=report&view=" + rid);
                    Utils.waitForLoad(d);


                }
            }
        }
    }

    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {
        timerScheduler = new Timer();
        timerTask = new TimerTask() {
            @Override
            public void run() {
                Tick();
            }
        };
        int dlay = (_CurrentDelay == Delay.Long ? _DelayLong : (_CurrentDelay == Delay.Medium ? _DelayMedium : _DelayLow));
        timerScheduler.scheduleAtFixedRate(timerTask, 0, dlay);

        tribalWars.Screen(TribalWars.Screens.Place);
        Utils.waitForLoad(tribalWars.driver);
        BotVillage nbv = new BotVillage(tribalWars.resources.villages.getFirst());
        tribalWars.resources.villages.getFirst().UpdateDeep();
        Utils.waitForLoad(tribalWars.driver);
        villages.add(nbv);
        nbv.SaveVillage();

        System.out.println("[SpBot] Registered new village!");
    }


    public enum  Delay {
        Long,
        Medium,
        Low
    }
}
