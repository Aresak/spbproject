package com.spb.frame.AccCreator;

import com.spb.frame.Utils;
import com.sun.corba.se.impl.logging.UtilSystemException;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.Random;

/**
 * Created by ares7 on 19.02.2017.
 */
public class AccCreate {
    String emailName = "";
    String emailPassword = "";
    String tribalWarsName = "";
    String tribalWarsPassword = "";
    String tribalWarsVerifyCode = "";
    WebDriver driver;

    public AccCreate(WebDriver driver) {
        this.driver = driver;
    }


    public void GenerateAccount() {
        driver.findElement(By.cssSelector("body")).sendKeys(Keys.chord(Keys.CONTROL, "t"));
        driver.findElement(By.cssSelector("body")).sendKeys(Keys.chord(Keys.CONTROL, Keys.TAB));
        Utils.waitForLoad(driver);

        // NameGen
        driver.navigate().to("http://fantasynamegenerators.com/pandaren_wow_names.php");
        Utils.waitForLoad(driver);
        String[] names = driver.findElement(By.id("result")).getText().split("\n");

        for(String name : names) {
            System.out.println(">> " + name + " (" + name.split(" ")[1] + ")");
        }

        Random r = new Random();
        String name = names[r.nextInt(names.length)].split(" ")[1];
        name += "" + (10 + r.nextInt(99));


        driver.navigate().to("https://registrace.seznam.cz/");
        Utils.waitForLoad(driver);

        driver.findElement(By.id("register-username")).sendKeys(name.toLowerCase());
        long password = r.nextLong() + 1000000000;

        List<WebElement> pwInputs = driver.findElements(By.tagName("input"));
        for(WebElement el : pwInputs) {
            if(!el.isDisplayed())
                continue;

            if(el.getAttribute("type").equals("password")) {
                el.sendKeys(Long.toString(password));
            }
        }

        driver.findElement(By.id("register-year")).sendKeys("2017");

        List<WebElement> regSpans = driver.findElements(By.tagName("span"));
        for(WebElement el : regSpans) {
            if(!el.isDisplayed())
                continue;

            if(el.getAttribute("data-locale") != null) {
                if(el.getAttribute("data-locale").equals("register.agree"))
                    el.click();
                else if(el.getAttribute("data-locale").equals("register.male"))
                    el.click();
            }
        }

        List<WebElement> regBtns = driver.findElements(By.tagName("button"));
        for(WebElement el : regBtns) {
            if(!el.isDisplayed())
                continue;

            if(el.getAttribute("data-locale") != null) {
                if(el.getAttribute("data-locale").equals("register.submit")) {
                    el.click();
                    System.out.println("Registered Email on email.cz\nEmail: " + name + "@seznam.cz\nPassword: " + password);
                }
            }
        }

        Utils.waitForLoad(driver);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        driver.navigate().to("https://email.seznam.cz/settings#/dlists");
        Utils.waitForLoad(driver);

        driver.findElement(By.id("save-all")).click();
        Utils.waitForLoad(driver);

        driver.navigate().to("https://www.divokekmeny.cz/page/new");
        Utils.waitForLoad(driver);

        driver.findElement(By.id("register_username")).sendKeys(name);
        driver.findElement(By.id("register_password")).sendKeys(Long.toString(password));
        driver.findElement(By.id("register_email")).sendKeys(name.toLowerCase() + "@seznam.cz");
        driver.findElement(By.id("terms")).click();
        driver.findElement(By.className("btn-register")).click();
        Utils.waitForLoad(driver);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        driver.navigate().to("https://email.seznam.cz/");
        Utils.waitForLoad(driver);

        int wt = 60;


        while(true) {
            try {
                (new WebDriverWait(driver, wt)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[contains(text(), Aktivace')]")));
                fetchKey();
                break;
            }
            catch(Exception e) {
                wt = 3600;
                driver.navigate().to("https://www.divokekmeny.cz/?welcome=1");
                Utils.waitForLoad(driver);
                driver.navigate().to("https://www.divokekmeny.cz/page/play/cs51");
                Utils.waitForLoad(driver);
                driver.navigate().to("https://cs51.divokekmeny.cz/game.php?screen=email_valid&direct=1&");
                Utils.waitForLoad(driver);
                driver.findElement(By.partialLinkText("e-mail")).click();
                Utils.waitForLoad(driver);
                driver.navigate().to("https://email.seznam.cz/");
                Utils.waitForLoad(driver);
                break;
            }
        }


        Utils.waitForLoad(driver);


    }

    private void fetchKey() throws Exception {
        driver.findElement(By.xpath("//*[contains(text(), 'Aktivace')]")).click();
        Utils.waitForLoad(driver);

        driver.findElement(By.partialLinkText("POTVRDIT")).click();
    }
}
